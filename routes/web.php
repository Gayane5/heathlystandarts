<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::group(['prefix' => 'authorization', 'as' => 'authorization.'], function () {
            Route::get('/index', 'AuthorizationController@index')->name('index');
            Route::get('/getItemInfo/{id}', 'AuthorizationController@getItemInfo')->name('getItemInfo');
            Route::get('/changeStatus/{id}', 'AuthorizationController@changeStatus')->name('changeStatus');
        });
    });


    Route::group(['namespace' => 'Guest'], function () {
        Route::get('/patients/order/getItems/{id}', 'PatientController@getItems')->name('delivery.getItems');
        Route::get('/patients/order/getItemDetail/{id}', 'PatientController@getItemDetail')->name('delivery.getItemDetail');
        Route::get('/patients/order/getKitDetail/{id}', 'PatientController@getKitDetail')->name('delivery.getKitDetail');

        Route::get('/home', 'OrdersController@index')->name('home');
        Route::get('/currentOrders', 'OrdersController@index')->name('current_orders');
        Route::get('/currentOrders/cancel/{id}', 'OrdersController@cancel')->name('cancel_order');
        Route::get('/currentOrders/delivery/edit/{id}', 'OrdersController@editOrder')->name('edit_order');

        Route::group(['prefix' => 'patients', 'as' => 'patients.'], function () {
            Route::get('/', 'PatientController@index')->name('index');
            Route::get('/history/{id}', 'PatientController@history')->name('history');
            Route::get('/create', 'PatientController@create')->name('create');
            Route::get('/edit/{id}', 'PatientController@edit')->name('edit');
            Route::post('/store', 'PatientController@store')->name('store');
            Route::post('/update/{id}', 'PatientController@update')->name('update');
            Route::post('/address/store', 'PatientController@storeAddress')->name('address.store');
            Route::post('/address/update', 'PatientController@updateAddress')->name('address.update');
            Route::get('/address/edit/{id}', 'PatientController@editAddress')->name('address.edit');
            Route::post('/medgroup/store', 'PatientController@storeMedGroup')->name('medgroup.store');
            Route::post('/medgroup/update', 'PatientController@updateMedGroup')->name('medgroup.update');
            Route::get('/medgroup/edit/{id}', 'PatientController@editMedGroup')->name('medgroup.edit');
            Route::post('/diagnose/store', 'PatientController@storeDiagnose')->name('diagnose.store');
            Route::get('/diagnose/description/{code}', 'PatientController@getDiagnoseDescription')->name('diagnose.description');
            Route::post('/diagnose/update', 'PatientController@updateDiagnose')->name('diagnose.update');
            Route::get('/diagnose/edit/{id}', 'PatientController@editDiagnose')->name('diagnose.edit');
            Route::get('/diagnose/remove/{id}', 'PatientController@removeDiagnose')->name('diagnose.remove');
            Route::get('/medgroup/remove/{id}', 'PatientController@removeMedgroup')->name('medgroup.remove');
            Route::post('/doctor/store', 'PatientController@storeDoctor')->name('doctor.store');
            Route::get('/doctor/edit/{id}', 'PatientController@editDoctor')->name('doctor.edit');
            Route::post('/doctor/update', 'PatientController@updateDoctor')->name('doctor.update');
            Route::get('/delete/{id}', 'PatientController@delete')->name('delete');
            Route::group(['prefix' => '{patient_id}/order', 'as' => 'order.'], function () {
                Route::get('/pickup/create', 'PatientController@pickupCreate')->name('pickup.create');
                Route::get('/respite/create', 'PatientController@respiteCreate')->name('respite.create');
                Route::get('/move/create', 'PatientController@moveCreate')->name('move.create');
                Route::get('/exchange/create', 'PatientController@exchangeCreate')->name('exchange.create');
                Route::get('/delivery/create', 'PatientController@deliveryCreate')->name('delivery.create');
                Route::get('/delivery/store', 'PatientController@storeDelivery')->name('delivery.store');
                Route::get('/delivery-edit/store', 'OrdersController@updateDelivery')->name('delivery-edit.store');
                Route::get('/delivery/getItems/{id}', 'PatientController@getItems')->name('delivery.getItems');
            });
//            Route::group(['prefix' => 'addresses', 'as' => 'address.'], function (){
//                Route::post('/store', 'PatientController@storeAddress')->name('store');
//                Route::post('/update/{id}', 'PatientController@updateAddress')->name('update');
//            });
//            Route::group(['prefix' => 'doctor', 'as' => 'doctor.'], function (){
//                Route::post('/store', 'PatientController@storeDoctor')->name('store');
//                Route::post('/update/{id}', 'PatientController@updateDoctor')->name('update');
//            });
//            Route::group(['prefix' => 'medGroup', 'as' => 'medgroup.'], function (){
//                Route::post('/store', 'PatientController@storeMedGroup')->name('store');
//                Route::post('/update/{id}', 'PatientController@updateMedGroup')->name('update');
//            });
//            Route::group(['prefix' => 'diagnose', 'as' => 'diagnose.'], function (){
//                Route::post('/store', 'PatientController@storeDiagnose')->name('store');
//                Route::post('/update/{id}', 'PatientController@updateDiagnose')->name('update');
//            });
        });

        Route::group(['prefix' => 'customerCare', 'as' => 'customer_care.'], function () {
            Route::get('/', 'CustomerController@index')->name('index');
            Route::get('/create_concern', 'CustomerController@createConcern')->name('create_concern');
            Route::get('/create_compliment', 'CustomerController@createCompliment')->name('create_compliment');
        });

        Route::group(['prefix' => 'facilities', 'as' => 'facilities.'], function () {
            Route::get('/', 'FacilitiesController@index')->name('index');
            Route::get('/create', 'FacilitiesController@create')->name('create');
            Route::get('/edit/{id}', 'FacilitiesController@edit')->name('edit');
        });

        Route::group(['prefix' => 'nurses', 'as' => 'nurses.'], function () {
            Route::get('/', 'NursesController@index')->name('index');
            Route::get('/create', 'NursesController@create')->name('create');
            Route::get('/edit/{id}', 'NursesController@edit')->name('edit');
            Route::post('/store', 'NursesController@store')->name('store');
            Route::post('/update/{id}', 'NursesController@update')->name('update');
            Route::get('/delete/{id}', 'NursesController@delete')->name('delete');
        });

        Route::get('/account', 'AccountController@index')->name('account');
        Route::get('/resources', 'ResourcesController@index')->name('resources');
    });
});

Route::get('confirmation/{token}', ['uses' => 'Admin\AdminController@changePassword']);