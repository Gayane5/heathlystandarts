 function onNext(parent, panel) {
        hash = "#" + panel.id;
        $(".acc-wizard-sidebar", $(parent))
            .children("li")
            .children("a[href='" + hash + "']")
            .parent("li")
            .removeClass("acc-wizard-todo")
    }
    $(window).load(function() {
        $('.acc-wizard-step').css('display:none');
        $(".acc-wizard").accwizard({
            onNext: onNext
        });
    });