@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/pages/todolist.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/pages/tab.css')}}"/>
@endsection

@section('content')
    <aside>
        <section class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="todolist">
                        <form class="row list_of_items">
                            @foreach($authorizations as $item)
                                <div class="todolist_list showactions {{ ($item->Status == 'Completed') ? 'completed_item' : '' }}">
                                    <div class="col-md-8 col-sm-8 col-xs-12 nopadmar custom_textbox1">
                                        <div class="todoitemcheck">
                                            <input data-id="{{$item->AuthID}}" type="checkbox"
                                                   class="striked" {{ ($item->Status == 'Completed') ? 'disabled' : '' }}/>
                                        </div>
                                        <div class="todotext todoitem"><a href="#" style="color:black"
                                                                          data-order="{{json_encode($item->authItems)}}"
                                                                          class="accordion">{{$item->AuthID}} {{$item->patient ? $item->patient->PFirstName : ''}} {{$item->patient ? $item->patient->PLastName : ''}}</a>
                                            <div class="panel panel-left-items" style="display:none;">
                                                @if(isset($item->authItems))
                                                    @foreach($item->authItems as $wItem)
                                                        <ul>
                                                            <li>
                                                                Count: {{$wItem->Qty}} - {{$wItem->CPTDescription}}
                                                            </li>
                                                        </ul>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4  col-sm-4 col-xs-8  pull-right showbtns todoitembtns">
                                        @if($item->Status != 'Completed')
                                            <a href="#" class="todoedit change_status" data-id="{{$item->AuthID}}">
                                                <span class="glyphicon glyphicon-check"></span>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-success" style="min-height: 150px;">
                        <div class="panel-body">
                            <div class="bs-example">
                                <ul class="nav nav-pills nav-stacked ware_house_info" style="max-width: 300px;"></ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-body">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                @if(isset($insurances))
                                    @foreach($insurances as $key => $insurance)
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading_{{$key}}">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapse_{{$key}}" aria-expanded="true"
                                                   aria-controls="collapse_{{$key}}">
                                                    <h4 class="panel-title">{{$insurance->InsName}}</h4>
                                                </a>
                                            </div>
                                            <div id="collapse_{{$key}}" class="panel-collapse collapse"
                                                 role="tabpanel"
                                                 aria-labelledby="heading_{{$key}}">

                                                <div class="panel-body">
                                                    <div class="panel-group" id="accordion_patient" role="tablist"
                                                         aria-multiselectable="true">
                                                        @if(isset($insurance->authorization))
                                                            @foreach($insurance->authorization->groupBy('PatID') as $key => $authorization)
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading" role="tab"
                                                                         id="heading_patient_{{$key}}">
                                                                        <a role="button" data-toggle="collapse"
                                                                           data-parent="#accordion_patient"
                                                                           href="#collapse_patient_{{$key}}"
                                                                           aria-expanded="true"
                                                                           aria-controls="collapse_patient_{{$key}}">
                                                                            <h4 class="panel-title">{{$authorization[0]->patient->PFirstName}} {{$authorization[0]->patient->PLastName}}</h4>
                                                                        </a>
                                                                    </div>
                                                                    <div id="collapse_patient_{{$key}}"
                                                                         class="panel-collapse collapse"
                                                                         role="tabpanel"
                                                                         aria-labelledby="heading_patient_{{$key}}">

                                                                        <div class="panel-body">
                                                                            <div class="panel-group"
                                                                                 id="accordion_order" role="tablist"
                                                                                 aria-multiselectable="true">
                                                                                @foreach($authorization as $key => $item)
                                                                                    <div class="panel panel-default">
                                                                                        <div class="panel-heading"
                                                                                             role="tab"
                                                                                             id="heading_order_{{$item->AuthID}}">
                                                                                            <a role="button"
                                                                                               data-toggle="collapse"
                                                                                               data-parent="#accordion_order"
                                                                                               data-id="{{$item->AuthID}}"
                                                                                               href="#collapse_order_{{$item->AuthID}}"
                                                                                               aria-expanded="true"
                                                                                               aria-controls="collapse_order_{{$item->AuthID}}">
                                                                                                <h4 class="panel-title">
                                                                                                    Order: {{$item->AuthID}}</h4>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div id="collapse_order_{{$item->AuthID}}"
                                                                                             class="panel-collapse collapse"
                                                                                             role="tabpanel"
                                                                                             aria-labelledby="heading_order_{{$item->AuthID}}">

                                                                                            <div class="panel-body">
                                                                                                <div class="panel-group"
                                                                                                     id="accordion_item"
                                                                                                     role="tablist"
                                                                                                     aria-multiselectable="true">
                                                                                                    @foreach($item->authItems as $key => $authItem)
                                                                                                        <div class="panel panel-default">
                                                                                                            <div class="panel-heading"
                                                                                                                 role="tab"
                                                                                                                 id="heading_item_{{$key}}">
                                                                                                                <a class="show_item_info {{in_array($authItem->AuthItemID, $left_items_array) ? 'in_left_array' : ""}}"
                                                                                                                   role="button"
                                                                                                                   data-id="{{$authItem->AuthItemID}}"
                                                                                                                   href="#">
                                                                                                                    <h4 style="background-color: {{in_array($authItem->AuthItemID, $left_items_array) && ($qty[$authItem->AuthItemID] - $authItem->Qty) <=0 ? 'green' : 'white'}}"
                                                                                                                        class="panel-title">{{$authItem->CPTDescription}}
                                                                                                                        - {{in_array($authItem->AuthItemID, $left_items_array) ? ($qty[$authItem->AuthItemID] - $authItem->Qty) : $authItem->Qty}}</h4>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
    </aside>

    <div class="modal fade" id="kit_modal" style="z-index: 1000000" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="kit_modal_title"></h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12" id="kit_modal_description">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('/js/pages/todolist.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/pages/tabs_accordions.js')}}"></script>
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }

        $(document).on('click', '.show_item_info', function () {
            var id = $(this).data('id');
            var url = "{{route('admin.authorization.getItemInfo', ":id")}}";
            url = url.replace(':id', id);
            $.ajax({
                method: 'GET',
                url: url,
                success: function (data) {
                    $('.ware_house_info').empty();
                    var row = "";
                    $.each(data, function (key, value) {
                        row = row + '<li><a href="#" data-id="' + value.WHItemID + '">' + value.SerialNO + ' - ' + value.ItemNotes + '</a></li>';
                    });
                    $('.ware_house_info').append(row);
                }
            });
        });

        $('.show_ordered_items').click(function () {
            $('#kit_modal_description').html("");
            var id = $(this).data('id');
            var order = $(this).data('order');
            var kit_row = '';
            $.each(order, function (key, value) {
                kit_row = kit_row + 'Count: ' + value.Qty + ' ' + value.CPTDescription + '<br>';
            });
            $('#kit_modal_description').append(kit_row);
            // $('#kit_modal').modal('show');
        });

        $(document).on('click', '.change_status', function () {
            var this_item = $(this);
            var id = $(this).data('id');
            var url = "{{route('admin.authorization.changeStatus', ":id")}}";
            url = url.replace(':id', id);
            $.ajax({
                method: 'GET',
                url: url,
                success: function (data) {
                    this_item.css({'display': 'none'});
                    this_item.parents('.todolist_list').addClass('completed_item');
                    $('input[data-id="' + id + '"]').attr('disabled', 'disabled');
                }
            });
        });
    </script>
@endsection
