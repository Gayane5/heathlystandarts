@extends('layouts.app')

@section('styles')
    <link href="css/pages/tables.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
            {{--<!--section starts-->--}}
            {{--<h1>Resources</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li>--}}
                    {{--<a href="{{route('home')}}">--}}
                        {{--<i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="active">Resources</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        <!--section ends-->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box primary">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="livicon" data-name="folders" data-size="16" data-loop="true" data-c="#fff"
                                   data-hc="white"></i>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Resource</th>
                                        <th>Link</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Bed Selection Criteria</td>
                                        <td><a href="#">Click to download</a></td>
                                    </tr>
                                    <tr>
                                        <td>Driver Leave Behind</td>
                                        <td><a href="#">Click to download</a></td>
                                    </tr>
                                    <tr>
                                        <td>HMS User Guide</td>
                                        <td><a href="#">Click to download</a></td>
                                    </tr>
                                    <tr>
                                        <td>Mattress Support Surface Eligibility</td>
                                        <td><a href="#">Click to download</a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <div>
                                <h3>Videos</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <iframe width="100%" height="345" src="https://www.youtube.com/embed/tgbNymZ7vqY">
                                            </iframe>
                                        </div>
                                        <div class="col-md-6">
                                            <iframe width="100%" height="345" src="https://www.youtube.com/embed/tgbNymZ7vqY">
                                            </iframe>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </section>
        <!-- content -->
    </aside>
    <!-- right-side -->
@endsection
