@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('css/vendors/bootstrap-tagsinput/bootstrap-tagsinput.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/pages/blog.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/datatables/dataTables.bootstrap.css')}}"/>
    <link href="{{asset('css/pages/tables.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
    {{--<section class="content-header">--}}
    {{--<!--section starts-->--}}
    {{--<h1>Patients</h1>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li>--}}
    {{--<a href="{{route('home')}}">--}}
    {{--<i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="{{route('patients.index')}}">--}}
    {{--<i class="livicon" data-name="users" data-size="14" data-loop="true"></i> Patients--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--<li class="active">History</li>--}}
    {{--</ol>--}}
    {{--</section>--}}
    <!--section ends-->

        <section class="content">
            <!--main content-->
            <div class="row">
                <div class="col-md-12 col-full-width-right">
                    <!-- /.blog-detail-image -->
                    <div class="the-box no-border blog-detail-content" style="margin-bottom: 0;">
                        <p>
                            <span class="label label-danger square" style="font-size:24px">{{$patient->PLastName}} {{$patient->PFirstName}} </span>
                            <span style="margin-left: 15px;font-size: 16px"><b>MedicalID:</b> {{$patient->PMedicalID}}</span>
                            <span style="margin-left: 15px;font-size: 16px"><b>DOB:</b> {{date('m/d/Y', strtotime($patient->PDOB))}}</span>
                        </p>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-10">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab"  aria-expanded=true href="#address">Address</a></li>
                                        <li><a data-toggle="tab" href="#doctor">Doctor</a></li>
                                        <li><a data-toggle="tab" href="#medgroup">MedGroup</a></li>
                                        <li><a data-toggle="tab" href="#diagnoses">Diagnoses</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div id="address" class="tab-pane fade in active">
                                            <h3></h3>
                                            <table class="table table-hover" id="patient_address_table">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Address</th>
                                                    <th>City</th>
                                                    <th>State</th>
                                                    <th>Zip</th>
                                                    <th>Phone</th>
                                                </tr>
                                                </thead>
                                                <tbody id="order_previus_table">
                                                @foreach($patient->addresses as $address)
                                                    <tr id="{{$address->PaddrID}}">
                                                        <td>{{$address->Paddrname}}</td>
                                                        <td>{{$address->PAddress}}</td>
                                                        <td>{{$address->Pcity}}</td>
                                                        <td>{{$address->PState}}</td>
                                                        <td>{{$address->PZip}}</td>
                                                        <td>{{$address->PPhone}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="doctor" class="tab-pane fade">
                                            <h3></h3>
                                            <table class="table table-hover" id="patient_doctor_table">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Type</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($patient->doctors as $doctor)
                                                    @if(isset($doctor->doctor))
                                                        <tr id="{{$doctor->PatientDoctorID}}">
                                                            <td>{{$doctor->doctor->DocFirstName}} {{$doctor->doctor->DocLastName}}</td>
                                                            <td>{{$doctor->doctor->DocEmail}}</td>
                                                            <td>{{$doctor->doctor->DocPhone}}</td>
                                                            <td>{{$doctor->doctor->Type}}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="medgroup" class="tab-pane fade">
                                            <h3></h3>
                                            <table class="table table-hover" id="patient_medgroup_table">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($patient->medGroups as $item)
                                                    @if(isset($item->medGroup))
                                                        <tr id="{{$item->patMedGroupID}}">
                                                            <td>{{$item->medGroup->medGroupName}}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="diagnoses" class="tab-pane fade">
                                            <h3></h3>
                                            <table class="table table-hover" id="patient_diagnose_table">
                                                <thead>
                                                <tr>
                                                    <th>Code</th>
                                                    <th>Description</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($patient->diagnoses as $item)
                                                    @if(isset($item->diagnose))
                                                        <tr id="{{$item->PatDiagnoseID}}">
                                                            <td>{{$item->diagnose->DiagnoseCode}}</td>
                                                            <td>{{$item->diagnose->Description}}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <blockquote>
                            <small>
                                <a href="{{route('patients.edit', $patient->PatID)}}">Update this patient's profile
                                    » </a>
                            </small>
                        </blockquote>

                    </div>
                </div>
            </div>
            <div class="row" style="padding:0">
                <div class="col-md-12 col-full-width-right">
                    <div class="the-box no-border blog-detail-content" style="margin-bottom: 0;padding:0">

                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet box primary" style="width: auto;margin:0">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="livicon" data-name="" data-size="16" data-loop="true" data-c="#fff"
                                       data-hc="white"></i> Orders
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Order Date</th>
                                            <th>Order #</th>
                                            <th>Dispatched</th>
                                            <th>Status</th>
                                            <th>Completed</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($orders))
                                            @foreach($orders as $order)
                                                <tr>
                                                    <td>{{$order->AuthDate}}</td>
                                                    <td><a href="#" data-order="{{json_encode($order->authItems)}}" data-order="{{json_encode($order)}}" class="show_ordered_items">{{$order->AuthID}}</a></td>
                                                    <td></td>
                                                    <td>{{$order->Status}}</td>
                                                    <td></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>

            <div class="row"  style="padding:0">
                <div class="col-md-12 col-full-width-right">
                    <div class="the-box no-border blog-detail-content" style="margin-bottom: 0;padding:0"
                    >

                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet box primary" style="width: auto;margin:0">
                            <div class="portlet-title" style="background-color: red">
                                <div class="caption">
                                    <i class="livicon" data-name="" data-size="16" data-loop="true" data-c="#fff"
                                       data-hc="white"></i> Inventory
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Delivery Date</th>
                                            <th>Address</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{--@foreach($inventory as $item)--}}
                                            {{--<tr>--}}
                                                {{--<td>{{$item->DeliveryDate}}</td>--}}
                                                {{--<td>{{$item->address->Paddrname}}</td>--}}
                                            {{--</tr>--}}
                                        {{--@endforeach--}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>

            <div class="row"  style="padding:0">
                <div class="col-md-12 col-full-width-right">
                    <div class="the-box no-border blog-detail-content" style="margin-bottom: 0;padding:0">

                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet box primary" style="width: auto;margin:0">
                            <div class="portlet-title" style="background-color: green">
                                <div class="caption">
                                    <i class="livicon" data-name="" data-size="16" data-loop="true" data-c="#fff"
                                       data-hc="white"></i> Deliveries
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Delivery Date</th>
                                            <th>Address</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{--@foreach($inventory as $item)--}}
                                            {{--<tr>--}}
                                                {{--<td>{{$item->DeliveryDate}}</td>--}}
                                                {{--<td>{{$item->address->Paddrname}}</td>--}}
                                            {{--</tr>--}}
                                        {{--@endforeach--}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </aside>
    @include('guest.orders.modals.ordered_items')
    <!-- right-side -->
@endsection

@section('scripts')
    <script src="{{asset('js/vendors/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/table-responsive.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.show_ordered_items').click(function () {
                $('#kit_modal_description').html("");
                var id = $(this).data('id');
                var order=$(this).data('order');
                var kit_row = '';
                $.each(order, function (key, value) {
                    kit_row = kit_row + 'Count: ' + value.Qty + ' ' +  value.CPTDescription + '<br>';
                });
                $('#kit_modal_description').append(kit_row);
                $('#kit_modal').modal('show');
            });
        });
    </script>
@endsection