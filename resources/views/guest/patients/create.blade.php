@extends('layouts.app')

@section('styles')
    <link href="{{asset('css/vendors/bootstrap-multiselect/bootstrap-multiselect.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('css/vendors/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/select2/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/pages/customform_elements.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/vendors/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('css/vendors/iCheck/all.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
    {{--<section class="content-header">--}}
    {{--<!--section starts-->--}}
    {{--<h1>Add New Patient</h1>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li>--}}
    {{--<a href="{{route('home')}}">--}}
    {{--<i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="{{route('patients.index')}}">--}}
    {{--<i class="livicon" data-name="users" data-size="14" data-loop="true"></i> Patients--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--<li class="active">Add new patient</li>--}}
    {{--</ol>--}}
    {{--</section>--}}
    <!--section ends-->
        <section class="content">
            <!--main content-->
            <div class="row">
                <!--row starts-->
                <div class="col-md-12">
                    <div class="panel panel-info" id="hidepanel2">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="plus" data-size="16" data-loop="true" data-c="#fff"
                                   data-hc="white"></i>
                            </h3>
                            <span class="pull-right">
                                    <i class="glyphicon glyphicon-chevron-up clickable"></i>
                                </span>
                        </div>
                        <div class="panel-body" style="text-align: center;">
                            {!! Form::open(array('route' => 'patients.store', 'method' => 'post')) !!}
                            {{ csrf_field() }}
                            <fieldset>
                                <!-- Name input-->
                                <div class="row">
                                    <div class="form-group {{ $errors->has('PFirstName') ? ' has-error' : '' }}">
                                        {!! Form::label('PFirstName','First Name*', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('PFirstName', null, ['class' => 'form-control', 'id' => 'PFirstName', 'placeholder' => 'First Name']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PFirstName') }}</small></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Email input-->
                                <div class="row">
                                    <div class="form-group {{ $errors->has('PLastName') ? ' has-error' : '' }}">
                                        {!! Form::label('PLastName','Last Name*', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('PLastName', null, ['class' => 'form-control', 'id' => 'PLastName', 'placeholder' => 'Last Name']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PLastName') }}</small></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Date of birth-->
                                <div class="row">
                                    <div class="{{ $errors->has('PDOB') ? ' has-error' : '' }}">
                                        {!! Form::label('PDOB','Date Of Birth*', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="livicon" data-name="calendar" data-size="14"
                                                       data-loop="true"></i>
                                                </div>
                                                {!! Form::text('PDOB', null, ['class' => 'select2-target form-control datetime4']) !!}
                                            </div>
                                            <span class="help-block">
                                                <small>{{ $errors->first('PDOB') }}</small></span></div>
                                    </div>
                                    <div class="{{ $errors->has('PAdmited') ? ' has-error' : '' }}">
                                        {!! Form::label('PAdmited','Admited*', ['class' => 'control-label col-md-1']) !!}
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="livicon" data-name="calendar" data-size="14"
                                                       data-loop="true"></i>
                                                </div>
                                                {!! Form::text('PAdmited', null, ['class' => 'select2-target form-control datetime4']) !!}
                                            </div>
                                            <span class="help-block">
                                                <small>{{ $errors->first('PAdmited') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('PediatricPatient') ? ' has-error' : '' }}">
                                        {!! Form::label('PediatricPatient','Pediatric
                                        Patient', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-2" style="text-align: left">
                                            {!! Form::checkbox('PediatricPatient', 1) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PediatricPatient') }}</small></span>
                                        </div>
                                    </div>


                                    <div class="form-group {{ $errors->has('PLastName') ? ' has-error' : '' }}">
                                        {!! Form::label('PSex','Sex*', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-2">
                                            {!! Form::select('PSex', \App\Models\Patients::$sex ,null, ['class' => 'select2-target form-control']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('PSex') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="{{ $errors->has('PHeight') ? ' has-error' : '' }}">
                                        {!! Form::label('PHeight','Patient Height', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-2">
                                            {!! Form::number('PHeight', null, ['class' => 'form-control', 'id' => 'PHeight', 'placeholder' => 'Height', 'min'=>0,'step' => '0.1']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PHeight') }}</small></span>
                                        </div>
                                    </div>

                                    <div class="{{ $errors->has('PHeight') ? ' has-error' : '' }}">
                                        {!! Form::label('PWeight','Patient Weight', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-2">
                                            {!! Form::number('PWeight', null, ['class' => 'form-control', 'id' => 'PWeight', 'placeholder' => 'Weight', 'min' => 0]) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PWeight') }}</small></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="{{ $errors->has('POxygenLiterID') ? ' has-error' : '' }}">
                                        {!! Form::label('POxygenLiterID','Oxygen Liter Flow', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-2">
                                            {!! Form::select('POxygenLiterID', $oxygenLiterFlow ,null, ['class' => 'select2-target form-control', 'id' => 'select32']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('POxygenLiterID') }}</small></span></div>
                                    </div>

                                    <div class="{{ $errors->has('Insulin') ? ' has-error' : '' }}">
                                        {!! Form::label('Insulin','Insulin', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-2">
                                            {!! Form::number('Insulin' ,null, ['class' => 'select2-target form-control', 'style' => 'width:50%']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('Insulin') }}</small></span></div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="{{ $errors->has('PCPAP') ? ' has-error' : '' }}">
                                        {!! Form::label('PCPAP','Patient CPAP', ['class' => 'control-label col-md-2 ']) !!}
                                        <div class="col-md-2">
                                            {!! Form::select('PCPAP', \App\Models\Patients::$BIPAP ,null, ['class' => 'select2-target form-control', 'id' => 'select36']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('PCPAP') }}</small></span></div>
                                    </div>

                                    <div>
                                        {!! Form::label('BIPAP','BIPAP', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-1 {{ $errors->has('PIPAP') ? ' has-error' : '' }}">
                                            {!! Form::number('PIPAP' ,null, ['class' => 'select2-target form-control',  'style'=>"width: 150%"]) !!}
                                            <p>IPAP</p>
                                            <span class="help-block">
                                                <small>{{ $errors->first('PIPAP') }}</small></span></div>

                                        <div class="col-md-1 {{ $errors->has('PEPAP') ? ' has-error' : '' }}">
                                            {!! Form::number('PEPAP' ,null, ['class' => 'select2-target form-control',  'style'=>"width: 150%"]) !!}
                                            <p>EPAP</p>
                                            <span class="help-block">
                                                <small>{{ $errors->first('PEPAP') }}</small></span></div>

                                        <div class="col-md-1 {{ $errors->has('PRate') ? ' has-error' : '' }}">
                                            {!! Form::number('PRate' ,null, ['class' => 'select2-target form-control',  'style'=>"width: 150%"]) !!}
                                            <p>Rate (if applicable)</p>
                                            <span class="help-block">
                                                <small>{{ $errors->first('PRate') }}</small></span></div>
                                    </div>
                                </div>

                                {{--<div class="row">--}}
                                {{--<div class="form-group {{ $errors->has('PDoctor') ? ' has-error' : '' }}">--}}
                                {{--{!! Form::label('PDoctor','Doctor', ['class' => 'control-label col-md-2']) !!}--}}
                                {{--<div class="col-md-2">--}}
                                {{--{!! Form::select('PDoctor', $doctors ,null, ['class' => 'select2-target form-control', 'id' => 'select38']) !!}--}}
                                {{--<span class="help-block">--}}
                                {{--<small>{{ $errors->first('PDoctor') }}</small></span></div>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="row">--}}
                                {{--<div class="form-group {{ $errors->has('PMedGroup') ? ' has-error' : '' }}">--}}
                                {{--{!! Form::label('PMedGroup','MedGroup', ['class' => 'control-label col-md-2']) !!}--}}
                                {{--<div class="col-md-2">--}}
                                {{--{!! Form::select('PMedGroup', $medGroup ,null, ['class' => 'select2-target form-control', 'id' => 'select39']) !!}--}}
                                {{--<span class="help-block">--}}
                                {{--<small>{{ $errors->first('PMedGroup') }}</small></span></div>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                <div class="row">
                                    <div class="form-group {{ $errors->has('PMedicalID') ? ' has-error' : '' }}">
                                        {!! Form::label('PMedicalID','MedicalID', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('PMedicalID', null, ['class' => 'form-control', 'id' => 'PMedicalID ', 'placeholder' => 'MedicalID ']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('PMedicalID') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('PLanguage') ? ' has-error' : '' }}">
                                        {!! Form::label('PLanguage','Language', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('PLanguage', null, ['class' => 'form-control', 'id' => 'PLanguage', 'placeholder' => 'Language']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('PLanguage') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('CareGiverInfo') ? ' has-error' : '' }}">
                                        {!! Form::label('CareGiverInfo','Care Giver Info', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('CareGiverInfo', null, ['class' => 'form-control', 'id' => 'CareGiverInfo', 'placeholder' => 'Care Giver Info']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('CareGiverInfo') }}</small></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <h3> {!! Form::label('PEC','Emergency Contact
                                                    Information', ['class' => 'control-label col-md-6']) !!}</h3>
                                    </div>
                                </div>

                                <div class="row {{ $errors->has('PEMopt') ? ' has-error' : '' }}">
                                    <div class="form-group">
                                        {!! Form::label('PEMopt','Emergency Opt
                                            Out', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-4">
                                            {!! Form::checkbox('PEMopt', 1) !!}
                                            I'm choosing not to provide an Emergency Contact at this time
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PEMopt') }}</small></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('PEMContact') ? ' has-error' : '' }}">
                                        {!! Form::label('PEMContact','Emergency Contact
                                      ', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('PEMContact', null, ['class' => 'form-control', 'id' => 'PEMContact', 'placeholder' => 'Emergency Contact']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PEMContact') }}</small></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('PEMPhone') ? ' has-error' : '' }}">
                                        {!! Form::label('PEMPhone','Emergency Phone
                                      ', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('PEMPhone', null, ['class' => 'form-control', 'id' => 'PEMPhone', 'placeholder' => 'Emergency Phone']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PEMPhone') }}</small></span>
                                        </div>
                                    </div>
                                </div>

                                <!-- Form actions -->
                                <div class="form-group">
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-responsive btn-info btn-sm">Save
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- content -->
    </aside>
    <!-- right-side -->
@endsection

@section('scripts')
    <script src="{{asset('js/vendors/bootstrap-multiselect/bootstrap-multiselect.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('js/vendors/moment/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/daterangepicker/daterangepicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/datetimepicker/bootstrap-datetimepicker.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('js/vendors/select2/select2.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/pages/custom_elements.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/iCheck/icheck.js')}}"></script>
    <script src="{{asset('js/pages/form_examples.js')}}"></script>
    <script>
        $('.datetime4').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    </script>
@endsection