<form class="form-horizontal" action="#" method="post">
    <fieldset>
        <!-- Name input-->
        <div class="form-group">
            <label class="col-md-3 control-label" for="firstname">First Name</label>
            <div class="col-md-6">
                <input id="firstname" name="firstname" type="text"
                       placeholder="Enter your First Name"
                       class="form-control"></div>
        </div>
        <!-- Email input-->
        <div class="form-group">
            <label class="col-md-3 control-label" for="lastname">Last Name</label>
            <div class="col-md-6">
                <input id="lastname" name="lastname" type="lastname"
                       placeholder="Enter your Last Name" class="form-control"></div>
        </div>
        <!-- Date of birth-->
        <div class="form-group">
            <label class="col-md-3 control-label" for="birthday">Date Of Birth</label>
            <div class="col-md-2">
                <select id="select21" class="form-control select2">
                    <option value="">Select month...</option>
                    <option value="AZ">January</option>
                    <option value="CO">February</option>
                    <option value="ID">March</option>
                    <option value="MT">April</option>
                    <option value="NE">May</option>
                    <option value="NM">June</option>
                    <option value="UT">July</option>
                    <option value="WY">August</option>
                    <option value="AL">September</option>
                    <option value="AR">October</option>
                    <option value="IL">November</option>
                    <option value="IA">December</option>
                </select>
            </div>
            <div class="col-md-2">
                <select id="select22" class="form-control select2">
                    <option value="">Select day...</option>
                    <option value="AZ">1</option>
                    <option value="CO">2</option>
                    <option value="ID">3</option>
                    <option value="MT">4</option>
                    <option value="NE">5</option>
                    <option value="NM">6</option>
                    <option value="UT">7</option>
                    <option value="WY">8</option>
                    <option value="AL">9</option>
                    <option value="AR">10</option>
                    <option value="IL">11</option>
                    <option value="IA">12</option>
                    <option value="AZ">13</option>
                    <option value="CO">14</option>
                    <option value="ID">15</option>
                    <option value="MT">16</option>
                    <option value="NE">17</option>
                    <option value="NM">18</option>
                    <option value="UT">19</option>
                    <option value="WY">20</option>
                    <option value="AL">21</option>
                    <option value="AR">22</option>
                    <option value="IL">23</option>
                    <option value="IA">24</option>
                    <option value="ID">25</option>
                    <option value="MT">26</option>
                    <option value="NE">27</option>
                    <option value="NM">28</option>
                    <option value="UT">29</option>
                    <option value="WY">30</option>
                    <option value="AL">31</option>
                </select>
            </div>
            <div class="col-md-2">
                <select id="select25" class="form-control select2">
                    <option value="">Select year...</option>
                    <option value="1900">1900</option>
                    <option value="1900">1901</option>
                    <option value="1900">1902</option>
                    <option value="1900">1903</option>
                    <option value="1900">1904</option>
                    <option value="1900">1905</option>
                    <option value="1900">1906</option>
                    <option value="1900">1907</option>
                    <option value="1900">1908</option>
                    <option value="1900">1909</option>
                    <option value="1900">1910</option>
                    <option value="1900">1911</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="pediatric_patient">Pediatric Patient</label>
            <div class="col-md-6">
                <input type="checkbox" name= " pediatric_patient" class="custom-checkbox">
            </div>
        </div>

        <!-- Location-->
        <div class="form-group">
            <label class="col-md-3 control-label" for="birthday">Location Type</label>
            <div class="col-md-2">
                <select id="select24" class="form-control select2">
                    <option value="">Select...</option>
                    <option value="AZ">Home</option>
                    <option value="CO">Group Home</option>
                    <option value="ID">Assisted Living</option>
                    <option value="MT">Facility</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="birthday">Facility Name</label>
            <div class="col-md-2">
                <select id="select26" class="form-control select2">
                    <option value="">Select...</option>
                    <option value="AZ">Country Villa Los Feliz Nursing Center</option>
                    <option value="CO">Devey Apartaments</option>
                    <option value="ID">Assisted Living</option>
                    <option value="MT">Facility</option>
                    <option value="AZ">Home</option>
                    <option value="CO">Group Home</option>
                    <option value="ID">Assisted Living</option>
                    <option value="MT">Facility</option>
                </select>
            </div>
        </div>

        <div class="cloneContent">

        </div>

        <div id="clone_content">
        <div class="form-group">
            <label class="col-md-3 control-label" for="address1">Address*</label>
            <div class="col-md-6">
                <input id="address1" name="address1" type="text"
                       placeholder="Enter your Address"
                       class="form-control"></div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="address2">Address(cont)</label>
            <div class="col-md-6">
                <input id="address2" name="address2" type="text"
                       placeholder="Enter your Address"
                       class="form-control"></div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="city">City</label>
            <div class="col-md-6">
                <input id="city" name="city" type="text" placeholder="Enter your City"
                       class="form-control"></div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="state">State</label>
            <div class="col-md-2">
                <select id="select28" class="form-control select2">
                    <option value="">Select...</option>
                    <option value="AZ">CA</option>
                    <option value="CO">AL</option>
                    <option value="ID">AK</option>
                    <option value="MT">AZ</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="zip">Zip Code</label>
            <div class="col-md-6">
                <input id="zip" name="zip" type="text" placeholder="Enter zip code"
                       class="form-control"></div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="phone">Phone</label>
            <div class="col-md-6">
                <input id="phone" name="phone" type="text" placeholder="Enter Phone"
                       class="form-control"></div>
        </div>
        </div>
        <div class="form-group">
        <div class="col-md-3">
        </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-default" id="add_address" onclick="cloneAddress();">Add Address</button>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="diagnosis">Diagnosis</label>
            <div class="col-md-6">
                <input id="diagnosis" name="diagnosis" type="text"
                       placeholder="Enter Diagnosis"
                       class="form-control"></div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="state">Patient Height*</label>
            <div class="col-md-2">
                <select id="select29" class="form-control select2">
                    <option value="">Select...</option>
                    <option value="AZ">Feet</option>
                    <option value="AZ">1</option>
                    <option value="CO">2</option>
                    <option value="ID">3</option>
                    <option value="MT">4</option>
                    <option value="AZ">5</option>
                    <option value="CO">6</option>
                    <option value="ID">7</option>
                    <option value="MT">8</option>
                </select>
            </div>
            <div class="col-md-2">
                <select id="select30" class="form-control select2">
                    <option value="">Select...</option>
                    <option value="AZ">Inches</option>
                    <option value="AZ">1</option>
                    <option value="CO">2</option>
                    <option value="ID">3</option>
                    <option value="MT">4</option>
                    <option value="AZ">5</option>
                    <option value="CO">6</option>
                    <option value="ID">7</option>
                    <option value="MT">8</option>
                    <option value="CO">9</option>
                    <option value="ID">10</option>
                    <option value="MT">11</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="patient_weight">Patient
                Weight*</label>
            <div class="col-md-6">
                <input id="patient_weight" name="patient_weight" type="text"
                       placeholder="Enter Patient Weight"
                       class="form-control"></div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="state">Oxygen Liter Flow</label>
            <div class="col-md-2">
                <select id="select31" class="form-control select2">
                    <option value="">Select...</option>
                    <option value="AZ">1</option>
                    <option value="CO">2</option>
                    <option value="ID">3</option>
                    <option value="MT">4</option>
                    <option value="AZ">5</option>
                    <option value="CO">6</option>
                    <option value="ID">7</option>
                    <option value="MT">8</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="state">CPAP Settings</label>
            <div class="col-md-2">
                <select id="select32" class="form-control select2">
                    <option value="">Select...</option>
                    <option value="AZ">1</option>
                    <option value="CO">2</option>
                    <option value="ID">3</option>
                    <option value="MT">4</option>
                    <option value="AZ">5</option>
                    <option value="CO">6</option>
                    <option value="ID">7</option>
                    <option value="MT">8</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="state">BIPAP Settings</label>
            <div class="col-md-2" style="text-align: center">
                <select id="select33" class="form-control select2">
                    <option value="">Select...</option>
                    <option value="AZ">1</option>
                    <option value="CO">2</option>
                    <option value="ID">3</option>
                    <option value="MT">4</option>
                    <option value="AZ">5</option>
                    <option value="CO">6</option>
                    <option value="ID">7</option>
                    <option value="MT">8</option>
                </select>
                <p>IPAP</p>
            </div>

            <div class="col-md-2" style="text-align: center">
                <select id="select34" class="form-control select2">
                    <option value="">Select...</option>
                    <option value="AZ">1</option>
                    <option value="CO">2</option>
                    <option value="ID">3</option>
                    <option value="MT">4</option>
                    <option value="AZ">5</option>
                    <option value="CO">6</option>
                    <option value="ID">7</option>
                    <option value="MT">8</option>
                </select>
                <p>EPAP</p>
            </div>

            <div class="col-md-2" style="text-align: center">
                <select id="select35" class="form-control select2">
                    <option value="">Select...</option>
                    <option value="AZ">1</option>
                    <option value="CO">2</option>
                    <option value="ID">3</option>
                    <option value="MT">4</option>
                    <option value="AZ">5</option>
                    <option value="CO">6</option>
                    <option value="ID">7</option>
                    <option value="MT">8</option>
                </select>
                <p>Rate (if applicable)</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-5 control-label" for=""><h3><b>Emergency Contact
                        Information</b></h3>
            </label>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="emergency_opt_out">Emergency Opt Out</label>
            <div class="col-md-6">
                <input type="checkbox" name= " emergency_opt_out" class="custom-checkbox"> I'm choosing not to provide an Emergency Contact at this time
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="emergency_contact">Emergency Contact</label>
            <div class="col-md-6">
                <input id="emergency_contact" name="emergency_contact" type="text"
                       placeholder="Enter Emergency Contact"
                       class="form-control"></div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="emergency_phone">
                Emergency Phone</label>
            <div class="col-md-6">
                <input id="emergency_phone" name="emergency_phone" type="text"
                       placeholder="Enter Emergency Phone"
                       class="form-control"></div>
        </div>
        <!-- Form actions -->
        <div class="form-group">
            <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-responsive btn-info btn-sm">Save
                </button>
            </div>
        </div>

    </fieldset>
</form>
