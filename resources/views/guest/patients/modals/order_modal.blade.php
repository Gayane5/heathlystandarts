<div class="modal fade" id="order_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="order_modal_title"></h4>
            </div>
            <div class="modal-body" style="padding-top:0">
                <div class="panel-body" style="padding-top:0">
                    <form role="form" class="order_form" id="order_form">

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-responsive btn-default update_order" >Update Order</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>