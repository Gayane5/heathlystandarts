<div class="modal fade" id="doctor_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="order_modal_title">Add new Doctor</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    {!! Form::open(array('route' => 'patients.doctor.store', 'class'=>"form_doctor")) !!}
                    <input type="hidden" name="PatID" id="add_doctor_PatID">
                    <div class="row">
                        <div class="form-group {{ $errors->has('PDoctor') ? ' has-error' : '' }}">
                            {!! Form::label('PDoctor*','DocID', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-6">
                                {!! Form::select('DocID', $doctors ,null, ['class' => 'select2-target form-control', 'id' => 'add_doctor_DocID']) !!}
                                <span class="help-block">
                                                <small>{{ $errors->first('PDoctor') }}</small></span></div>
                        </div>
                    </div>

                    <div style="float: right;">
                        <button type="button" class="btn btn-responsive btn-default add_new_doctor">Add Doctor</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="edit_doctor_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="order_modal_title">Edit Doctor</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    {!! Form::open(array('route' => 'patients.doctor.store', 'method' => 'post', 'class'=>"edit_form_doctor")) !!}
                    <input type="hidden" name="PatID" id="edit_doctor_PatID">
                    <input type="hidden" name="PatientDoctorID" id="edit_doctor_PatientDoctorID">
                    <div class="row">
                        <div class="form-group {{ $errors->has('PDoctor') ? ' has-error' : '' }}">
                            {!! Form::label('PDoctor','DocID', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-6">
                                {!! Form::select('DocID', $doctors ,null, ['class' => 'select2-target form-control', 'id' => 'edit_doctor_DocID']) !!}
                                <span class="help-block">
                                                <small>{{ $errors->first('PDoctor') }}</small></span></div>
                        </div>
                    </div>

                    <div style="float: right;">
                        <button type="button" class="btn btn-responsive btn-default update_doctor">Save Changes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>