<div class="modal fade" id="addresses_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="order_modal_title">Add new address</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    {!! Form::open(array('route' => 'patients.address.store', 'method' => 'post', 'class'=>"form_address")) !!}
                    <input type="hidden" name="PatID" id="PatID">
                    <input type="hidden" name="Status" value="1">
                    <div class="row">
                        <div class="{{ $errors->has('Paddrname') ? ' has-error' : '' }}">
                            {!! Form::label('Paddrname','Address Name*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                {!! Form::text('Paddrname', null, ['class' => 'form-control', 'id' => 'Paddrname', 'placeholder' => 'Enter your Address']) !!}
                                <span class="help-block"><small>{{ $errors->first('Paddrname') }}</small></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="{{ $errors->has('PAddress') ? ' has-error' : '' }}">
                            {!! Form::label('PAddress','Address*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                {!! Form::text('PAddress', null, ['class' => 'form-control', 'id' => 'PAddress', 'placeholder' => 'Enter your Address']) !!}
                                <span class="help-block">
                                                        <small>{{ $errors->first('PAddress') }}</small></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="{{ $errors->has('AddressNote') ? ' has-error' : '' }}">
                            {!! Form::label('AddressNote ','Address Notes*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                {!! Form::text('AddressNote', null, ['class' => 'form-control', 'id' => 'AddressNote', 'placeholder' => 'Enter Address Notes']) !!}
                                <span class="help-block">
                                                        <small>{{ $errors->first('AddressNote') }}</small></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="{{ $errors->has('PCity') ? ' has-error' : '' }}">
                            {!! Form::label('PCity','City*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                {!! Form::text('Pcity', null, ['class' => 'form-control', 'id' => 'PCity', 'placeholder' => 'City']) !!}
                                <span class="help-block">
                                                        <small>{{ $errors->first('PCity') }}</small></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="{{ $errors->has('PState') ? ' has-error' : '' }}">
                            {!! Form::label('PState','State*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-4">
                                {!! Form::select('PState', $states ,null, ['class' => 'select2-target form-control', 'id' => 'add_address_patient', 'placeholder' => 'State']) !!}
                                <span class="help-block">
                                                <small>{{ $errors->first('PState') }}</small></span></div>
                        </div>

                        <div class="{{ $errors->has('PZip') ? ' has-error' : '' }}">
                            {!! Form::label('PZip','Zip*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-4">
                                {!! Form::text('PZip', null, ['class' => 'form-control', 'id' => 'PZip', 'placeholder' => 'Zip']) !!}
                                <span class="help-block">
                                                        <small>{{ $errors->first('PZip') }}</small></span>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group {{ $errors->has('PPhone') ? ' has-error' : '' }}">
                            {!! Form::label('PPhone','Phone*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-4">
                                {!! Form::text('PPhone', null, ['class' => 'form-control', 'id' => 'PPhone', 'placeholder' => 'Phone']) !!}
                                <span class="help-block">
                                                        <small>{{ $errors->first('PPhone') }}</small></span>
                            </div>
                        </div>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-responsive btn-default add_new_address">Add Address</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" name="edit_addresses_modal" id="edit_addresses_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="order_modal_title">Edit address</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    {!! Form::open(array('route' => 'patients.address.store', 'method' => 'post', 'class'=>"form_address_update")) !!}
                    <input type="hidden" name="PatID" id="edit_address_PatID">
                    <input type="hidden" name="PaddrID" id="edit_address_PaddrID">
                    <input type="hidden" name="Status" id="edit_address_Status" value="1">
                    <div class="row">
                        <div class="{{ $errors->has('Paddrname') ? ' has-error' : '' }}">
                            {!! Form::label('Paddrname','Address Name*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                {!! Form::text('Paddrname', null, ['class' => 'form-control', 'id' => 'edit_address_Paddrname', 'placeholder' => 'Enter your Address']) !!}
                                <span class="help-block"><small>{{ $errors->first('Paddrname') }}</small></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="{{ $errors->has('PAddress') ? ' has-error' : '' }}">
                            {!! Form::label('PAddress','Address*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                {!! Form::text('PAddress', null, ['class' => 'form-control', 'id' => 'edit_address_PAddress', 'placeholder' => 'Enter your Address']) !!}
                                <span class="help-block">
                                                        <small>{{ $errors->first('PAddress') }}</small></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="{{ $errors->has('AddressNote') ? ' has-error' : '' }}">
                            {!! Form::label('AddressNote ','Address Notes*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                {!! Form::text('AddressNote', null, ['class' => 'form-control', 'id' => 'edit_address_AddressNote', 'placeholder' => 'Enter Address Notes']) !!}
                                <span class="help-block">
                                                        <small>{{ $errors->first('AddressNote') }}</small></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="{{ $errors->has('PCity') ? ' has-error' : '' }}">
                            {!! Form::label('PCity','City*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                {!! Form::text('Pcity', null, ['class' => 'form-control', 'id' => 'edit_address_Pcity', 'placeholder' => 'City']) !!}
                                <span class="help-block">
                                                        <small>{{ $errors->first('PCity') }}</small></span>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="{{ $errors->has('PState') ? ' has-error' : '' }}">
                            {!! Form::label('PState','State*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-4">
                                {!! Form::select('PState', $states ,null, ['class' => 'form-control', 'id' => 'edit_address_PState', 'placeholder' => 'State']) !!}
                                <span class="help-block">
                                                <small>{{ $errors->first('PState') }}</small></span></div>
                        </div>

                        <div class="{{ $errors->has('PZip') ? ' has-error' : '' }}">
                            {!! Form::label('PZip','Zip*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-4">
                                {!! Form::text('PZip', null, ['class' => 'form-control', 'id' => 'edit_address_PZip', 'placeholder' => 'Zip']) !!}
                                <span class="help-block">
                                                        <small>{{ $errors->first('PZip') }}</small></span>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group {{ $errors->has('PPhone') ? ' has-error' : '' }}">
                            {!! Form::label('PPhone','Phone*', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-4">
                                {!! Form::text('PPhone', null, ['class' => 'form-control', 'id' => 'edit_address_PPhone', 'placeholder' => 'Phone']) !!}
                                <span class="help-block">
                                                        <small>{{ $errors->first('PPhone') }}</small></span>
                            </div>
                        </div>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-responsive btn-default update_address">Save Changes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>