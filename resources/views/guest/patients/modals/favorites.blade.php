<div class="modal fade" id="favoritesModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Equipment List Favorites</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form id="form-prerequisites">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <img src="{{asset('images/order/folder_favs.png')}}">
                                </div>
                            </div>
                        </div><hr>
                        <p>
                            You can add to your Favorites list by clicking product names in
                            the Ordering area!
                        </p>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>