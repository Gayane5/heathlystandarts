<div class="modal fade" id="item_modal" style="z-index: 10000" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="item_modal_title"></h4>
            </div>
            <div class="modal-body" style="padding-bottom: 0">
                <div class="panel-body" style="padding-bottom: 0">
                    <div class="row">
                        <div class="col-md-3" id="item_modal_image">
                        </div>
                        <div class="col-md-9" id="item_modal_description" style="margin:60px 0">
                        </div>
                    </div>
                    <hr>
                    <b>Includes below items:</b>
                    <ul class="order_form" id="item_form">

                    </ul>
                </div>
            </div>
            <div class="modal-footer"  style="padding: 0">
                <button type="button" class="btn btn-default close_kit">Close</button>
            </div>
        </div>
    </div>
</div>