<div class="modal fade" id="medgroup_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="order_modal_title">Add new MedGroup</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    {!! Form::open(array('route' => 'patients.medgroup.store', 'method' => 'post', 'class' => 'medgroup_form')) !!}
                    <input type="hidden" name="PatID" id="medgroup_PatID">
                    <div class="row">
                        <div class="form-group {{ $errors->has('PMedGroup') ? ' has-error' : '' }}">
                            {!! Form::label('PMedGroup','medGroupID', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-6">
                                {!! Form::select('medGroupID', $medGroup ,null, ['class' => 'select2-target form-control', 'id' => 'add_medgroup_medGroupID']) !!}
                                <span class="help-block">
                                                <small>{{ $errors->first('PMedGroup') }}</small></span></div>
                        </div>
                    </div>

                    <div style="float: right;">
                        <button type="button" class="btn btn-responsive btn-default add_new_medgroup">Add MeddGroup</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="edit_medgroup_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="order_modal_title">Edit MedGroup</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    {!! Form::open(array('route' => 'patients.medgroup.store', 'method' => 'post', 'class' => 'edit_medgroup_form')) !!}
                    <input type="hidden" name="PatID" id="edit_medgroup_PatID">
                    <input type="hidden" name="patMedGroupID" id="edit_medgroup_patMedGroupID">
                    <div class="row">
                        <div class="form-group {{ $errors->has('PMedGroup') ? ' has-error' : '' }}">
                            {!! Form::label('PMedGroup','medGroupID', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-6">
                                {!! Form::select('medGroupID', $medGroup ,null, ['class' => 'select2-target form-control', 'id' => 'edit_medgroup_medGroupID']) !!}
                                <span class="help-block">
                                                <small>{{ $errors->first('PMedGroup') }}</small></span></div>
                        </div>
                    </div>

                    <div style="float: right;">
                        <button type="button" class="btn btn-responsive btn-default update_medgroup">Save Changes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>