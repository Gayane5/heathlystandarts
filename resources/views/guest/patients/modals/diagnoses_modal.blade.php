<div class="modal fade" id="diagnoses_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="order_modal_title">Add new diagnose</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    {!! Form::open(array('route' => 'patients.diagnose.store', 'method' => 'post', 'class' => 'form_diagnose')) !!}
                    <input type="hidden" name="PatID" id="diagnose_PatID">
                    <div class="row">
                        <div class="form-group {{ $errors->has('PMedGroup') ? ' has-error' : '' }}">
                            {!! Form::label('Diagnoses','DiagnoseCode', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-6">
                                {!! Form::select('DiagnoseCode', $diagnoses ,null, ['class' => 'select2-target diagnose_select form-control', 'id' => 'DiagnoseCode']) !!}
                                <span class="help-block">
                                                <small>{{ $errors->first('PMedGroup') }}</small></span></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group {{ $errors->has('DiagnoseDescription') ? ' has-error' : '' }}">
                            {!! Form::label('Description','DiagnoseDescription', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-6">
                                {!! Form::text('DiagnoseDescription' ,$diagnose_desc->Description, ['class' => 'select2-target diagnose_description form-control', 'readonly']) !!}
                                <span class="help-block help-block-diagnose">
                                                <small>{{ $errors->first('DiagnoseDescription') }}</small></span></div>
                        </div>
                    </div>

                    <div style="float: right;">
                        <button type="button" class="btn btn-responsive btn-default add_new_diagnose">Add Diagnose</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="edit_diagnoses_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="order_modal_title">Edit Diagnose</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    {!! Form::open(array('route' => 'patients.diagnose.store', 'method' => 'post', 'class' => 'edit_form_diagnose')) !!}
                    <input type="hidden" name="PatID" id="edit_diagnose_PatID">
                    <input type="hidden" name="PatDiagnoseID" id="edit_diagnose_PatDiagnoseID">
                    <div class="row">
                        <div class="form-group {{ $errors->has('PMedGroup') ? ' has-error' : '' }}">
                            {!! Form::label('Diagnoses','DiagnoseCode', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-6">
                                {!! Form::select('DiagnoseCode', $diagnoses ,null, ['class' => 'select2-target diagnose_select form-control', 'id' => 'edit_diagnose_DiagnoseCode']) !!}
                                <span class="help-block">
                                                <small>{{ $errors->first('PMedGroup') }}</small></span></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group {{ $errors->has('DiagnoseDescription') ? ' has-error' : '' }}">
                            {!! Form::label('Description','Description', ['class' => 'control-label col-md-4']) !!}
                            <div class="col-md-6">
                                {!! Form::text('Description', $diagnose_desc->Description, ['class' => 'select2-target diagnose_description form-control', 'readonly']) !!}
                                <span class="help-block help-block-diagnose">
                                                <small>{{ $errors->first('DiagnoseDescription') }}</small></span></div>
                        </div>
                    </div>

                    <div style="float: right;">
                        <button type="button" class="btn btn-responsive btn-default update_diagnose">Save Changes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>