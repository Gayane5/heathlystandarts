<div class="modal fade" id="kit_modal" style="z-index: 1000000" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="kit_modal_title"></h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2" id="kit_modal_image">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12" id="kit_modal_description">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <form role="form" class="order_form" id="kit_form">

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>