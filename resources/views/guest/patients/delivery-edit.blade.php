@extends('layouts.app')

@section('styles')
    <link href="{{asset('css/vendors/bootstrap-multiselect/bootstrap-multiselect.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('css/vendors/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/select2/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/pages/customform_elements.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/acc-wizard/acc-wizard.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('css/pages/accordionformwizard.css')}}" rel="stylesheet"/>
    <style>
        .hover:hover {
            cursor: pointer;
        }

        #order_form .form-control {
            height: 20px;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-2 category-nav" style="max-width: 200px">
        @foreach($categories as $item)
            <div class="row">
                <div class="col-md-3">
                    <img style="float: left;" class="category-img"
                         src="data:image/jpeg;base64,{{base64_encode($item->CatPicture)}}">
                </div>
                <div class="col-md-4 category" style="text-align: center">
                    <a href="#" class="open_order_modal"
                       data-id="{{$item->CatID}}"
                       data-toggle="modal" data-target="#order_modal">{{$item->CatName}}</a>
                </div>
            </div>
            <hr style="margin: 0;">
        @endforeach
    </div>

    <aside class="right-side right-side-order">
        <div style="display:none;" class="alert alert-success  fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p class="text-center "></p>
        </div>
        <section class="content paddingleft_right15 content-header" data-id="{{$order->AuthID}}">
            <!--main content-->
            <div class="row">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="gear" data-size="16" data-loop="true" data-c="#fff"
                               data-hc="white"></i>
                        </h3>
                        <span class="pull-right clickable">
                                <i class="glyphicon glyphicon-chevron-up"></i>
                            </span>
                    </div>
                    <div class="panel-body">
                        <div class="row acc-wizard">
                            <div class="col-md-12">
                                <form id="submit_order_form">
                                    <div class="row">
                                        <div class="form-group {{ $errors->has('PDoctor') ? ' has-error' : '' }}">
                                            {!! Form::label('Address*','', ['class' => 'control-label col-md-2']) !!}
                                            <div class="col-md-8">
                                                {!! Form::select('PAddress', $addresses ,null, ['class' => 'select2-target form-control', 'id' => 'select37']) !!}
                                                <span class="help-block">
                            <small>{{ $errors->first('PAddress') }}</small></span></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group {{ $errors->has('PDoctor') ? ' has-error' : '' }}">
                                            {!! Form::label('Nurse/Doctor*','', ['class' => 'control-label col-md-2']) !!}
                                            <div class="col-md-8">
                                                {!! Form::select('PDoctor', $doctors ,$order->RefDocID, ['class' => 'select2-target form-control', 'id' => 'select38']) !!}
                                                <span class="help-block">
                                <small>{{ $errors->first('PDoctor') }}</small></span></div>
                                        </div>
                                    </div>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Item Count</th>
                                            <th>Item</th>
                                            <th>Included Items</th>
                                            <th>Price</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody id="order_previus_table">
                                        @if($order)
                                            <?php $total =0 ;?>
                                            @foreach($order->authItems as $item)
                                                <?php $total = $total + $item->item->itemCptMedGroup->CPTPrice;?>
                                                <tr><td><input type="hidden" class="selected_item" name="{{$item->item->ItemID}}" value="{{$item->Qty}}">{{$item->Qty}}</td>
                                                    <td><a class="open_item_modal" style="cursor:pointer" data-id="{{$item->item->ItemID}}">{{$item->item->ItemName}}</a>
                                                    <td>
                                                        @if($item->item->kits)
                                                            @foreach($item->item->kits as $kit)
                                                                <a href="#" data-id="{{$kit->kit->ItemName}}" class="open_kit_modal">{{$kit->kit->ItemName}}</a><br>
                                                                @endforeach
                                                            @endif
                                                    </td>
                                                    <td class="price"><span style="color:red">{{$item->item->itemCptMedGroup->CPTPrice > 0 ? $item->item->itemCptMedGroup->CPTPrice. '$' : ""}}</span></td>
                                                    <td><button class="btn btn-default delete-order-row" data-price="{{$item->item->itemCptMedGroup->CPTPrice}}$"  style="margin:5px 0 5px"><i class="fa fa-remove"></i></button></td>
                                                </tr>
                                            @endforeach
                                            <tr class="total_price" data-value="{{$total}}"><td colspan=3>Total Price</td><td colspan=2>{{$total}}$</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <button class="btn btn-default" type="button" id="submit_order"
                                            style="float: right" disabled>
                                        Submit Order
                                    </button>
                                    <div class="modal fade" id="confirm" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">
                                                        &times;
                                                    </button>
                                                </div>
                                                <div class="modal-body" style="text-align: center;">
                                                    <h4>Are you sure?</h4>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                    <a href="#" type="button"
                                                       class="btn btn-danger delete_confirm">Sure</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--main content ends-->
        </section>
        <!--moddal dialog -->
        <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="modal-1" role="dialog"
             aria-labelledby="modalLabelfade" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h4 class="modal-title" id="modalLabelfade"></h4>
                    </div>
                    <div class="modal-body">
                        <h3>
                            CONCENTRATOR 5 LITER KIT
                        </h3>
                        <p>
                            <img src="{{asset('images/order/1.jpg')}}">
                            Oxygen Concentrator provides up to 10LPM
                        </p>
                        <p>Includes:</p>
                        <ul>
                            <li>
                                O2 SWIVEL CONNECTOR
                            </li>
                            <li>
                                25FT EXT TUBING
                            </li>
                            <li>
                                CANNULA 7FT
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-raised btn-primary " data-toggle="modal" data-target="#modal-1">Add to
                            Favorites
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- content -->
    </aside>
    <!-- right-side -->
@endsection

@extends('guest.patients.modals.order_modal')
@extends('guest.patients.modals.item_modal')
@extends('guest.patients.modals.kit_modal')

@section('scripts')
    <script>
        $('document').ready(function () {

            if ($('#select37').has('option').length > 0 && $('#select38').has('option').length > 0
                && $('#order_previus_table tr').length >= 1) {
                $('#submit_order').removeAttr("disabled");
            }

            $(document).on('click', '.close_kit', function () {
                $('#item_modal').modal('hide');
            });
            $('.bed_accessories').click(function () {
                $('#bed_accessories').toggle();
            });
            $('.oxygen_accessories').click(function () {
                $('#oxygen_accessories').toggle();
            });
            $('.respiratory_service').click(function () {
                $('#respiratory_service').toggle();
            });

            $('.open_order_modal').click(function () {
                var id = $(this).data('id');
                var url = "{{route('delivery.getItems', ":id")}}";
                url = url.replace(':id', id);

                var selected = document.querySelectorAll('input.selected_item');
                if (selected.length != 0) {
                    var existing = [];
                    $.each(selected, function (index, value) {
                        existing[index] = parseInt(value.name);
                    });
                }
                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function (data) {
                        $('#order_modal_title').empty();
                        $('#order_form').empty();
                        $('#order_modal_image').empty();
                        $('#order_modal_title').data('name', data[0].CatName);
                        // var image = new Image();
                        // image.src = 'data:image/jpeg;base64,' + data[1];
                        // $('#order_modal_image').append('<img src="' + image.src + '">');

                        var order_title_html = 'Equipment List - ' + data[0].CatName;
                        var order_form_html = '';
                        var radio_exists = [];
                        $.each(data[0].sub_categories, function (key, value) {
                            order_form_html = order_form_html + '<hr><div class="form-group" style="margin:0;padding:0">\n' +
                                '                            <label style="margin:0;padding:0" data_id=' + value.SubCatID + '>' + value.SubCategoryName + '</label>\n';
                            var order_form_items = '';
                            var exists = '';
                            $.each(value.items, function (key2, value2) {
                                if (value2.ItemName.replace(/[. ,`'":.;/&()-]+/g, "-").substr(value2.ItemName.replace(/[. ,`'":.;/&()-]+/g, "-").length - 1) == '-') {
                                    var item_name = value2.ItemName.replace(/[. ,`'":.;/&()-]+/g, "-").slice(0, -1);
                                } else {
                                    var item_name = value2.ItemName.replace(/[. ,`'":.;/&()-]+/g, "-");
                                }
                                item_name = item_name.replace('-amp', '');
                                if (existing != undefined && jQuery.inArray(value2.ItemID, existing) >= 0) {
                                    exists = 'disabled';
                                    // if(value2.ItemParamType != 1){
                                    //     var cat_name = $('input[data-id="'+ value2.ItemID +'"]').data('cat');
                                    //     $('input[data-cat="' + cat_name + '"]').attr('disabled', 'disabled');
                                    // }
                                } else {
                                    exists = '';
                                }
                                $('#' + item_name).attr('data-kits', JSON.stringify(value2.kits));
                                order_form_items = order_form_items + '<div class="row" style="margin:0;padding:0">\n' +
                                    '                                <div class="col-md-2" style="margin:0;padding:0">\n' +
                                    (value2.ItemCountable == 1 ?
                                            ' <input type="number" name="' + value2.ItemName + '" class="form-control" min="0"\n' +
                                            '  max="30">\n' : ""
                                    ) +
                                    '                                </div>\n' +
                                    '                                <div class="col-md-10" style="margin:0;padding:0">\n' +
                                    '                                    <div style="margin:0;padding:0" class="' + (value2.ItemParamType == 1 ? 'checkbox' : 'radio') + '">\n' +
                                    '                                        <label>\n' +
                                    '                                            <input' + " " + exists + ' id="' + item_name + '" data-id="' + value2.ItemID + '" data-cat="' + value.SubCategoryName + '" type="' + (value2.ItemParamType == 1 ? 'checkbox' : 'radio') + '" class="custom-' + (value2.ItemParamType == 1 ? 'checkbox' : 'radio') + '"\n' +
                                    '                                                   name="' + (value2.ItemParamType == 1 ? value2.ItemName : value.SubCategoryName) + '" value="' + value2.ItemName + '"><a href="#"\n' +
                                    '                                                                      data-toggle="modal" data-id="' + value2.ItemID + '" data-price="' + (value2.item_cpt_med_group && value2.item_cpt_med_group.CPTPrice != 0 ? value2.item_cpt_med_group.CPTPrice + "$" : "") + '" data-kits="' + encodeURIComponent(JSON.stringify(value2.kits)) + '" class="open_item_modal ' + item_name + '" \n' +
                                    '                                                                      data-cat="' + value.SubCategoryName + '">' + value2.ItemName + '<span style="color:red">' + (value2.item_cpt_med_group && value2.item_cpt_med_group.CPTPrice != 0 ? '(' + value2.item_cpt_med_group.CPTPrice + '$)' : " ") + '</span></a> </label>\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                            </div>';

                                if (existing != undefined && jQuery.inArray(value2.ItemID, existing) >= 0) {
                                    if (value2.ItemParamType == 0) {
                                        radio_exists[value2.ItemID] = value2.ItemID;
                                    }
                                }
                            });
                            order_form_html = order_form_html + order_form_items + '</div>';
                        });

                        $('#order_modal_title').append(order_title_html);
                        $('#order_form').append(order_form_html);
                        console.log(radio_exists);
                        if (radio_exists.length != 0) {
                            $.each(radio_exists, function (index, value) {
                                var cat_name = $('.custom-radio[data-id="'+ value +'"]').data('cat');
                                $('.custom-radio[data-cat="' + cat_name + '"]').attr('disabled', 'disabled');
                            });
                        }
                        $('.order_modal').modal('show');
                    }
                });
            });

            $(document).on('change', 'input[type="number"]', function () {
                if (!$('input[value="' + $(this).attr("name") + '"]').is('[disabled=disabled]')) {
                    $('input[value="' + $(this).attr("name") + '"]').attr('checked', 'checked');
                }
            });

            $('.update_order').click(function () {
                var data = $('#order_form').serializeArray();
                var CategoryName = $('#order_modal_title').data('name');
                var order_table = '';

                var x = {};
                for (var k in data) {
                    if (x[data[k].name] == undefined)
                        x[data[k].name] = [];
                    x[data[k].name].push(data[k].value)
                }
                var total = 0;
                $.each(x, function (key, value) {
                    var kits = '';
                    var kit_row = '';

                    if ((value.length == 1 && value[0] && !$.isNumeric(value[0]))) {
                        console.log(222, value[0]);
                        if (value[0].replace(/[. ,`'":.;/&()-]+/g, "-").substr(value[0].replace(/[. ,`'":.;&/()-]+/g, "-").length - 1) == '-') {
                            var item_name = value[0].replace(/[. ,`'":.;&/()-]+/g, "-").slice(0, -1);
                        } else {
                            var item_name = value[0].replace(/[. ,`'":.;&/()-]+/g, "-");
                        }
                        console.log(3333, item_name);
                        kits = $("." + item_name).data('kits');
                        kits = JSON.parse(decodeURIComponent(kits));
                        $.each(kits, function (key, value) {
                            kit_row = kit_row + '<a href="#" data-id="' + value.kit.ItemID + '" class="open_kit_modal">' + value.kit.ItemName + '</a><br>';
                        });
                        if ($("." + item_name).data('price')) {
                            total = parseFloat(total) + parseFloat($("." + item_name).data('price').slice(0, -1));
                        }
                        ;
                        var item_id = $("." + item_name).data("id");
                        order_table = order_table + '<tr><td><input type="hidden" class="selected_item" name="' + $("." + item_name).data('id') + '" value="1">1</td><td><a class="open_item_modal" style="cursor:pointer" data-id="' + item_id + '">' + value[0] + '<a><td>' + kit_row + '</td><td class="price" data-price="' + $("." + item_name).data('price') + '"><span style="color:red">' + $("." + item_name).data('price') + '</span></td><td><button class="btn btn-default delete-order-row" data-price="' + $("." + item_name).data('price') + '" style="margin:5px 0 5px"><i class="fa fa-remove"></i></button></td></tr>'
                    } else if (value.length == 2 && value[1]) {
                        if (value[1].replace(/[. ,`'":.;&/()-]+/g, "-").substr(value[1].replace(/[. ,`'":.;&/()-]+/g, "-").length - 1) == '-') {
                            var item_name = value[1].replace(/[. ,`'":.;&/()-]+/g, "-").slice(0, -1);
                        } else {
                            var item_name = value[1].replace(/[. ,`'":.;&/()-]+/g, "-");
                        }
                        if ($("." + item_name).data('price')) {
                            total = parseFloat(total) + parseFloat($("." + item_name).data('price').slice(0, -1));
                        }
                        kits = $("." + item_name).data('kits');
                        kits = JSON.parse(decodeURIComponent(kits));
                        kit_row = '';
                        var item_id = $("." + item_name).data("id");
                        $.each(kits, function (key, value) {
                            kit_row = kit_row + '<a href="#" data-id="' + value.kit.ItemID + '" class="open_kit_modal">' + value.kit.ItemName + '</a><br>';
                        });
                        order_table = order_table + '<tr><td><input type="hidden" class="selected_item" name="' + $("." + item_name).data('id') + '" value="' + (value[0] == "" ? 1 : value[0]) + '">' + (value[0] == "" ? 1 : value[0]) + '</td><td><a class="open_item_modal" style="cursor:pointer" data-id="' + item_id + '">' + value[1] + '</a><td>' + kit_row + '</td><td class="price"  ><span style="color:red">' + $("." + item_name).data('price') + '<span></td><td><button class="btn btn-default delete-order-row" data-price="' + $("." + item_name).data('price') + '"  style="margin:5px 0 5px"><i class="fa fa-remove"></i></button></td></tr>'
                    }
                });
                if ($("tr").hasClass('total_price')) {
                    var old_total = $(".total_price").data('value');
                    total = parseFloat(total) + parseFloat(old_total);
                    $("tr.total_price").remove();
                }
                // }else{
                order_table = order_table + '<tr class="total_price" data-value="' + total + '"><td colspan=3><b>Total Price</b></td><td colspan=2>' + total + '$</td></tr>';
                // }
                $('#submit_order').removeAttr("disabled");
                $('#order_previus_table').append(order_table);
                $('#order_modal').modal('hide');
            });
        });

        $(document).on('click', '.delete-order-row', function () {
            // $('.active-delete').removeClass('active-delete');
            if ($("tr").hasClass('total_price')) {
                var old_total = $(".total_price").data('value');
                if ($(this).data('price')) {
                    total = parseFloat(old_total) - parseFloat($(this).data('price').slice(0, -1));
                    $("tr.total_price").remove();
                    if (total > 0) {
                        order_table = '<tr class="total_price" data-value="' + total + '"><td colspan=3>Total Price</td><td colspan=2>' + total + '$</td></tr>';
                        $('#order_previus_table').append(order_table);
                    }
                }
            }

            $(this).parents('tr').addClass('animated lightSpeedOut').remove();
            if ($('#order_previus_table tr').length < 1) {
                $('#submit_order').attr('disabled', 'disabled');
            }


            // }else{

            // $('.active-delete').addClass('animated lightSpeedOut');
            // setTimeout(function () {
            //     $('.active-delete').remove();
            // }, 800);
        });

        $(document).on('click', '#submit_order', function () {
            $('#confirm').modal('show');
        });

        $(document).on('click', '.delete_confirm', function () {
            var data = $('#submit_order_form').serializeArray();
            var id = $('.content-header').data('id');
            var url = "{{route('patients.order.delivery-edit.store', ":id")}}";
            url = url.replace(':id', id);
            $.ajax({
                method: 'GET',
                url: url,
                data: data,
                success: function (data) {
                    if (data == 'success') {
                        window.location.href = "/currentOrders";
                        $('.alert-success p').append('Order Successfully Updated!!!');
                        $('.alert-success').css({'display': 'block'});
                        $('#confirm').modal('hide');
                    }
                }
            });
        });

        $(document).on('click', '.open_item_modal', function () {
            var id = $(this).data('id');
            var url = "{{route('delivery.getItemDetail', ":id")}}";
            url = url.replace(':id', id);
            $.ajax({
                method: 'GET',
                url: url,
                success: function (data) {
                    $('#item_modal_title').empty();
                    $('#item_form').empty();
                    $('#item_modal_image').empty();
                    $('#item_modal_title').data('name', data[0].ItemName);
                    $('#item_modal_description').empty();
                    var image = new Image();
                    image.src = 'data:image/jpeg;base64,' + data[1];
                    $('#item_modal_image').append('<img src="' + image.src + '">');
                    var item_title_html = data[0].ItemName;
                    var item_modal_description = data[0].DetailedDescription;
                    var item_form_html = '';
                    $.each(data[0].kits, function (key, value) {
                        item_form_html = item_form_html + '<li><a style="color: black;" href="#" data-id="' + value.kit.ItemID + '" class="open_kit_modal">' + value.kit.ItemName + '</a></li>';
                    });
                    $('#item_modal_title').append(item_title_html);
                    $('#item_modal_description').append(item_modal_description);
                    $('#item_form').append(item_form_html);
                    $('#item_modal').modal('show');
                }
            });
        });

        $(document).on('click', '.open_kit_modal', function () {
            var id = $(this).data('id');
            var url = "{{route('delivery.getKitDetail', ":id")}}";
            url = url.replace(':id', id);
            $.ajax({
                method: 'GET',
                url: url,
                success: function (data) {
                    $('#kit_modal_title').empty();
                    $('#kit_modal_image').empty();
                    $('#kit_modal_title').data('name', data[0].ItemName);
                    $('#kit_modal_description').empty();
                    var image = new Image();
                    image.src = 'data:image/jpeg;base64,' + data[1];
                    $('#kit_modal_image').append('<img src="' + image.src + '">');
                    var item_title_html = data[0].ItemName;
                    var item_modal_description = data[0].DetailedDescription;
                    $('#kit_modal_title').append(item_title_html);
                    $('#kit_modal_description').append(item_modal_description);
                    $('#kit_modal').modal('show');
                }
            });
        });
    </script>
@endsection