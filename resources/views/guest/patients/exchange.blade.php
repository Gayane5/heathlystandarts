@extends('layouts.app')

@section('styles')
    <link href="{{asset('css/vendors/bootstrap-multiselect/bootstrap-multiselect.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('css/vendors/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/select2/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/pages/customform_elements.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
            {{--<!--section starts-->--}}
            {{--<h1>Create Order</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li>--}}
                    {{--<a href="{{route('home')}}">--}}
                        {{--<i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="{{route('patients.index')}}">--}}
                        {{--<i class="livicon" data-name="users" data-size="14" data-loop="true"></i> Patients--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="active">Create Order</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        <!--section ends-->
        <section class="content">
            <!--main content-->
            <div class="row">
                <!--row starts-->
                <div class="col-md-12">
                    <div class="panel panel-info" id="hidepanel2">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="plus" data-size="16" data-loop="true" data-c="#fff"
                                   data-hc="white"></i>
                            </h3>
                            <span class="pull-right">
                                    <i class="glyphicon glyphicon-chevron-up clickable"></i>
                                    <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                                </span>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" action="#" method="post">
                                <fieldset>
                                    <!-- Date of birth-->

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="pediatric_patient">Please select the items that need Exchanged</label>
                                        <div class="col-md-9">
                                            <input type="checkbox" name= " pediatric_patient" class="custom-checkbox">  CONCENTRATOR 5 LITER KIT<br>
                                            <input type="checkbox" name= " pediatric_patient" class="custom-checkbox">  BED FULL ELEC HI / LOW <br>
                                            <input type="checkbox" name= " pediatric_patient" class="custom-checkbox"> GEO MATTRESS <br>
                                            <input type="checkbox" name= " pediatric_patient" class="custom-checkbox">  BED RAILS- FULL LENGTH <br>
                                            <input type="checkbox" name= " pediatric_patient" class="custom-checkbox">  NEBULIZER <br>
                                            <input type="checkbox" name= " pediatric_patient" class="custom-checkbox">  WHEELCHAIR 16" <br>
                                            <input type="checkbox" name= " pediatric_patient" class="custom-checkbox"> WHEELCHAIR FOOTREST PR <br>
                                            <input type="checkbox" name= " pediatric_patient" class="custom-checkbox"> OVER BED TABLE <br>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="birthday">Instructions/Comments</label>
                                        <div class="col-md-6">
                                            <textarea name="instructions" class="form-control" rows="10"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="birthday">Ordering Nurse</label>
                                        <div class="col-md-3">
                                            <select id="select24" class="form-control select2">
                                                <option value="">Select...</option>
                                                <option value="AZ">Susanna Khechoyan - (818) 378-6182</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Location-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="birthday">Exchange Reason</label>
                                        <div class="col-md-3">
                                            <select id="select22" class="form-control select2">
                                                <option value="">Select...</option>
                                                <option value="AZ">Broken</option>
                                                <option value="CO">Parts Missing</option>
                                                <option value="ID">Needs Repair</option>
                                                <option value="MT">Quality</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Location-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="birthday">Please Choose Exchange Timing</label>
                                        <div class="col-md-9">
                                            <input type="radio" name= "pediatric_patient" value="same_day" class="custom-checkbox" checked>  Same Day Exchange <br>
                                            <input type="radio" name= "pediatric_patient" value="future" class="custom-checkbox">  Future Exchange <br>
                                        </div>
                                    </div>

                                    <div class="form-group" id="same_day_pickup">
                                        <label class="col-md-3 control-label" for="birthday"></label>
                                        <div class="col-md-3">
                                            <select id="select22" class="form-control select2">
                                                <option value="">Select...</option>
                                                <option value="AZ">Normal (4 hour)</option>
                                                <option value="CO">State (2 hour)</option>
                                                <option value="ID">After 2PM</option>
                                                <option value="MT">After 3PM</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" id="future_pickup" style="display: none">
                                        <div class="col-md-1"></div>
                                        <label class="col-md-2 control-label" for="birthday">Date</label>
                                        <div class="col-md-2">
                                            <select id="select22" class="form-control select2">
                                                <option value="AZ">11/10/18</option>
                                                <option value="CO">11/10/18</option>
                                                <option value="ID">11/10/18</option>
                                                <option value="MT">11/10/18</option>
                                            </select>
                                        </div>
                                        <label class="col-md-1 control-label" for="birthday">Time Range</label>
                                        <div class="col-md-2">
                                            <select id="select22" class="form-control select2">
                                                <option value="">Open...</option>
                                                <option value="AZ">9AM to 11AM</option>
                                                <option value="CO">9AM to 11AM</option>
                                                <option value="ID">9AM to 11AM</option>
                                                <option value="MT">9AM to 11AM</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Form actions -->
                                    <div class="form-group">
                                        <div class="col-md-12 text-right">
                                            <button type="submit" class="btn btn-responsive btn-info btn-sm">Save
                                            </button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- content -->
    </aside>
    <!-- right-side -->
@endsection

@section('scripts')
    <script src="{{asset('js/vendors/bootstrap-multiselect/bootstrap-multiselect.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('js/vendors/select2/select2.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/pages/custom_elements.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('input:radio[name="pediatric_patient"]').change(function(){
                if($(this).val() == 'same_day'){
                    $('#future_pickup').css('display','none');
                    $('#same_day_pickup').css('display','block');
                }else if($(this).val() == 'future'){
                    $('#same_day_pickup').css('display','none');
                    $('#future_pickup').css('display','block');
                }
            });

        });
    </script>
@endsection