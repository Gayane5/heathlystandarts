@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/datatables/dataTables.bootstrap.css')}}" />
    <link href="{{asset('css/pages/tables.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
            {{--<!--section starts-->--}}
            {{--<h1>Patients</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li>--}}
                    {{--<a href="index.html">--}}
                        {{--<i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="active">Patients</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        <!--section ends-->
        @if(session('success'))
            <div class="alert alert-success  fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <p class="text-center ">{{session('success')}}</p>
            </div>
        @endif
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box default">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Patients
                            </div>

                            <div class="caption" style="float: right;">
                                <a href="{{route('patients.create')}}" style="text-decoration: none;color: white;"><i class="livicon" data-name="plus-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Add New Patient</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Patient Name</th>
                                    <th>Date Of Birth</th>
                                    <th>Created</th>
                                    <th>Last Service</th>
                                    <th>MedicalID</th>
                                    <th>History</th>
                                    <th>New Order</th>
                                    {{--<th>Delete</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($patients as $patient)
                                    <tr data-id="{{$patient->PatID}}">
                                        <td><a href="{{route('patients.edit', $patient->PatID)}}">{{ $patient->PFirstName }} {{$patient->PLastName}}</a></td>
                                        <td>{{ date('m/d/Y', strtotime($patient->PDOB))}}</td>
                                        <td>{{ date('m/d/Y', strtotime($patient->PCreateDate)) }}</td>
                                        <td>{{ date('m/d/Y', strtotime($patient->PAdmited)) }}</td>
                                        <td>{{ $patient->PMedicalID }}</td>
                                        <td><a href="{{route('patients.history', $patient->PatID)}}">History</a></td>
                                        <td><button class="btn btn-raised btn-primary " data-toggle="modal" data-target="#modal-{{$patient->PatID}}">Create Order</button></td>
                                        <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="modal-{{$patient->PatID}}" role="dialog" aria-labelledby="modalLabelfade" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-primary">
                                                        <h4 class="modal-title" id="modalLabelfade">Create New Order</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h3>
                                                            Which type of order do you want to create?
                                                        </h3>
                                                        <ul>
                                                            <li>
                                                                <h4><a href="{{route('patients.order.delivery.create', $patient->PatID)}}">Delivery</a></h4>
                                                            </li>
                                                            <li>
                                                                <h4><a href="{{route('patients.order.exchange.create', $patient->PatID)}}">Exchange</a></h4>
                                                            </li>
                                                            <li>
                                                                <h4><a href="{{route('patients.order.move.create', $patient->PatID)}}">Move</a></h4>
                                                            </li>
                                                            <li>
                                                                <h4><a href="{{route('patients.order.respite.create', $patient->PatID)}}">Respite</a></h4>
                                                            </li>
                                                            <li>
                                                                <h4><a href="{{route('patients.order.pickup.create', $patient->PatID)}}">Pickup</a></h4>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </section>
        <!--moddal dialog -->

        <!-- content -->
    </aside>
    <!-- right-side -->
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/dataTables.responsive.js')}}"></script>
    <script src="{{asset('js/pages/table-responsive.js')}}"></script>
    <script>
        $(document).on('click','.delete-patient-row',function () {
            $('.active-delete').removeClass('active-delete');
            $(this).parents('tr').addClass('active-delete');
            $('#delete_confirm').modal('show');
            $('.delete_confirm').click(function () {
                var id = $('.active-delete').data('id');
                $.ajax({
                    method: 'get',
                    url: '/patients/delete/' + id,
                    success: function (data) {
                        if(data.success == null){
                            $('#delete_confirm').modal('hide');
                            $('.active-delete').addClass('animated lightSpeedOut');
                            setTimeout(function () {
                                $('.active-delete').remove();
                            },800);
                        }
                    }
                });
            });
        });
    </script>
@endsection