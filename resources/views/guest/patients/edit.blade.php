@extends('layouts.app')

@section('styles')
    <link href="{{asset('css/vendors/bootstrap-multiselect/bootstrap-multiselect.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('css/vendors/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/select2/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/pages/customform_elements.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/vendors/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection


@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
    {{--<section class="content-header">--}}
    {{--<!--section starts-->--}}
    {{--<h1>Edit Patient</h1>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li>--}}
    {{--<a href="{{route('home')}}">--}}
    {{--<i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="{{route('patients.index')}}">--}}
    {{--<i class="livicon" data-name="users" data-size="14" data-loop="true"></i> Patients--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--<li class="active">Edit patient</li>--}}
    {{--</ol>--}}
    {{--</section>--}}
    <!--section ends-->
        <section class="content">
            <!--main content-->
            <div class="row">
                <!--row starts-->
                <div class="col-md-12">
                    <div class="panel panel-info" id="hidepanel2">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="pencil" data-size="16" data-loop="true" data-c="#fff"
                                   data-hc="white"></i>
                            </h3>
                            <span class="pull-right">
                                    <i class="glyphicon glyphicon-chevron-up clickable"></i>
                                </span>
                        </div>
                        <div class="panel-body">
                            {!! Form::model($patient, ['method' => 'post', 'url' => route('patients.update', $patient->PatID)]) !!}
                            {{ csrf_field() }}
                            <fieldset class="PatID" data-id="{{$patient->PatID}}">
                                <!-- Name input-->
                                <div class="row">
                                    <div class="form-group {{ $errors->has('PFirstName') ? ' has-error' : '' }}">
                                        {!! Form::label('PFirstName','First Name*', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('PFirstName', null, ['class' => 'form-control', 'id' => 'PFirstName', 'placeholder' => 'First Name']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PFirstName') }}</small></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Email input-->
                                <div class="row">
                                    <div class="form-group {{ $errors->has('PLastName') ? ' has-error' : '' }}">
                                        {!! Form::label('PLastName','Last Name*', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('PLastName', null, ['class' => 'form-control', 'id' => 'PLastName', 'placeholder' => 'Last Name']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PLastName') }}</small></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Date of birth-->
                                <div class="row">
                                    <div class="{{ $errors->has('PDOB') ? ' has-error' : '' }}">
                                        {!! Form::label('PDOB','Date Of Birth*', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="livicon" data-name="calendar" data-size="14"
                                                       data-loop="true"></i>
                                                </div>
                                                {!! Form::text('PDOB', date('m/d/Y', strtotime($patient->PDOB)), ['class' => 'select2-target form-control datetime4']) !!}
                                            </div>
                                            <span class="help-block">
                                                <small>{{ $errors->first('PDOB') }}</small></span></div>
                                    </div>
                                    <div class="{{ $errors->has('PAdmited') ? ' has-error' : '' }}">
                                        {!! Form::label('PAdmited','Admited*', ['class' => 'control-label col-md-1' ]) !!}
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="livicon" data-name="calendar" data-size="14"
                                                       data-loop="true"></i>
                                                </div>
                                                {!! Form::text('PAdmited', date('m/d/Y', strtotime($patient->PAdmited)), ['class' => 'select2-target form-control datetime4']) !!}
                                            </div>
                                            <span class="help-block">
                                                <small>{{ $errors->first('PAdmited') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('PediatricPatient') ? ' has-error' : '' }}">
                                        {!! Form::label('PediatricPatient','Pediatric
                                        Patient', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-2">
                                            {!! Form::checkbox('PediatricPatient', 1) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PediatricPatient') }}</small></span>
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('PLastName') ? ' has-error' : '' }}">
                                        {!! Form::label('PSex','Sex*', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-2">
                                            {!! Form::select('PSex', \App\Models\Patients::$sex ,null, ['class' => 'select2-target form-control']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('PSex') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="{{ $errors->has('PHeight') ? ' has-error' : '' }}">
                                        {!! Form::label('PHeight','Patient Height', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-2">
                                            {!! Form::number('PHeight', null, ['class' => 'form-control', 'id' => 'PHeight', 'placeholder' => 'Height', 'min' => 0, 'step' => '0.1']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PHeight') }}</small></span>
                                        </div>
                                    </div>

                                    <div class="{{ $errors->has('PHeight') ? ' has-error' : '' }}">
                                        {!! Form::label('PWeight','Patient Weight', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-2">
                                            {!! Form::number('PWeight', null, ['class' => 'form-control', 'id' => 'PWeight', 'min' => 0, 'placeholder' => 'Weight']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PWeight') }}</small></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="{{ $errors->has('POxygenLiterID') ? ' has-error' : '' }}">
                                        {!! Form::label('POxygenLiterID','Oxygen Liter Flow', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-2">
                                            {!! Form::select('POxygenLiterID', $oxygenLiterFlow ,null, ['class' => 'select2-target form-control', 'id' => 'select32']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('POxygenLiterID') }}</small></span></div>
                                    </div>

                                    <div class="{{ $errors->has('Insulin') ? ' has-error' : '' }}">
                                        {!! Form::label('Insulin','Insulin', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-2">
                                            {!! Form::number('Insulin' ,null, ['class' => 'select2-target form-control',  'style'=>"width: 50%"]) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('Insulin') }}</small></span></div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="{{ $errors->has('PCPAP') ? ' has-error' : '' }}">
                                        {!! Form::label('PCPAP','Patient CPAP', ['class' => 'control-label col-md-2 ']) !!}
                                        <div class="col-md-2">
                                            {!! Form::select('PCPAP', \App\Models\Patients::$BIPAP ,null, ['class' => 'select2-target form-control', 'id' => 'select36']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('PCPAP') }}</small></span></div>
                                    </div>

                                    <div>
                                        {!! Form::label('BIPAP','BIPAP', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-1 {{ $errors->has('PIPAP') ? ' has-error' : '' }}">
                                            {!! Form::number('PIPAP' ,null, ['class' => 'select2-target form-control',  'style'=>"width: 150%"]) !!}
                                            <p>IPAP</p>
                                            <span class="help-block">
                                                <small>{{ $errors->first('PIPAP') }}</small></span></div>

                                        <div class="col-md-1 {{ $errors->has('PEPAP') ? ' has-error' : '' }}">
                                            {!! Form::number('PEPAP' ,null, ['class' => 'select2-target form-control',  'style'=>"width: 150%"]) !!}
                                            <p>EPAP</p>
                                            <span class="help-block">
                                                <small>{{ $errors->first('PEPAP') }}</small></span></div>

                                        <div class="col-md-1 {{ $errors->has('PRate') ? ' has-error' : '' }}">
                                            {!! Form::number('PRate' ,null, ['class' => 'select2-target form-control',  'style'=>"width: 150%"]) !!}
                                            <p>Rate (if applicable)</p>
                                            <span class="help-block">
                                                <small>{{ $errors->first('PRate') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('PMedicalID') ? ' has-error' : '' }}">
                                        {!! Form::label('PMedicalID','MedicalID', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('PMedicalID', null, ['class' => 'form-control', 'id' => 'PMedicalID ', 'placeholder' => 'MedicalID ']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('PMedicalID') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('PLanguage') ? ' has-error' : '' }}">
                                        {!! Form::label('PLanguage','Language', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('PLanguage', null, ['class' => 'form-control', 'id' => 'PLanguage', 'placeholder' => 'Language']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('PLanguage') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('CareGiverInfo') ? ' has-error' : '' }}">
                                        {!! Form::label('CareGiverInfo','Care Giver Info', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('CareGiverInfo', null, ['class' => 'form-control', 'id' => 'CareGiverInfo', 'placeholder' => 'CareGiverInfo']) !!}

                                            <span class="help-block">
                                                        <small>{{ $errors->first('CareGiverInfo') }}</small></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        {!! Form::label('','', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-10">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a data-toggle="tab" href="#address">Address</a></li>
                                                <li><a data-toggle="tab" href="#doctor">Doctor</a></li>
                                                <li><a data-toggle="tab" href="#medgroup">MedGroup</a></li>
                                                <li><a data-toggle="tab" href="#diagnoses">Diagnoses</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div id="address" class="tab-pane fade in active">
                                                    <h3></h3>
                                                    <table class="table table-hover" id="patient_address_table">
                                                        <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Address</th>
                                                            <th>City</th>
                                                            <th>State</th>
                                                            <th>Zip</th>
                                                            <th>Phone</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="order_previus_table">
                                                        @foreach($patient->addresses as $address)
                                                            <tr id="{{$address->PaddrID}}">
                                                                <td>{{$address->Paddrname}}</td>
                                                                <td>{{$address->PAddress}}</td>
                                                                <td>{{$address->Pcity}}</td>
                                                                <td>{{$address->PState}}</td>
                                                                <td>{{$address->PZip}}</td>
                                                                <td>{{$address->PPhone}}</td>
                                                                <td>
                                                                    <button class="edit_address" type="button"
                                                                            title="edit"
                                                                            data-id="{{$address->PaddrID}}"><i
                                                                                class="fa fa-pencil"></i>
                                                                    </button>
                                                                    {{--<a href="#" title="remove"--}}
                                                                    {{--data-id="{{$address->PaddrID}}"><i--}}
                                                                    {{--class="fa fa-remove remove_address_row"></i></a>--}}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    <button type="button" class="btn btn-default add_address"
                                                            style="float:right">Add
                                                        Address
                                                    </button>
                                                </div>
                                                <div id="doctor" class="tab-pane fade">
                                                    <h3></h3>
                                                    <table class="table table-hover" id="patient_doctor_table">
                                                        <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Email</th>
                                                            <th>Phone</th>
                                                            <th>Type</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($patient->doctors as $doctor)
                                                            @if(isset($doctor->doctor))
                                                                <tr id="{{$doctor->PatientDoctorID}}">
                                                                    <td>{{$doctor->doctor->DocFirstName}} {{$doctor->doctor->DocLastName}}</td>
                                                                    <td>{{$doctor->doctor->DocEmail}}</td>
                                                                    <td>{{$doctor->doctor->DocPhone}}</td>
                                                                    <td>{{$doctor->doctor->Type}}</td>
                                                                    <td>
                                                                        <button type="button" href="#"
                                                                                class="edit_doctor" title="edit_doctor"
                                                                                data-id="{{$doctor->PatientDoctorID}}">
                                                                            <i
                                                                                    class="fa fa-pencil"></i></button>
                                                                        {{--<a href="#" title="remoce"--}}
                                                                        {{--data-id="{{$doctor->PatientDoctorID}}"><i--}}
                                                                        {{--class="fa fa-remove"></i></a>--}}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    <button class="btn btn-default add_doctor" type="button"
                                                            style="float:right">Add
                                                        Doctor
                                                    </button>
                                                </div>
                                                <div id="medgroup" class="tab-pane fade">
                                                    <h3></h3>
                                                    <table class="table table-hover" id="patient_medgroup_table">
                                                        <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($patient->medGroups as $item)
                                                            @if(isset($item->medGroup))
                                                                <tr id="{{$item->patMedGroupID}}">
                                                                    <td>{{$item->medGroup->medGroupName}}</td>
                                                                    <td>
                                                                        <button type="button" href="#"
                                                                                class="edit_medgroup"
                                                                                title="edit_medgroup"
                                                                                data-id="{{$item->patMedGroupID}}"><i
                                                                                    class="fa fa-pencil"></i></button>
                                                                        <button type="button" href="#" class="remove_medgroup" title="remoce"
                                                                        data-id="{{$item->PatMedGroupID}}"><i
                                                                        class="fa fa-remove"></i></button>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    <button class="btn btn-default add_medGroup" type="button"
                                                            style="float:right">Add
                                                        MedGroup
                                                    </button>
                                                </div>
                                                <div id="diagnoses" class="tab-pane fade">
                                                    <h3></h3>
                                                    <table class="table table-hover" id="patient_diagnose_table">
                                                        <thead>
                                                        <tr>
                                                            <th>Code</th>
                                                            <th>Description</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($patient->diagnoses as $item)
                                                            @if(isset($item->diagnose))
                                                                <tr id="{{$item->PatDiagnoseID}}">
                                                                    <td>{{$item->diagnose->DiagnoseCode}}</td>
                                                                    <td>{{$item->diagnose->Description}}</td>
                                                                    <td>
                                                                        <button type="button" class="edit_diagnose"
                                                                                title="edit"
                                                                                data-id="{{$item->PatDiagnoseID}}"><i
                                                                                    class="fa fa-pencil"></i></button>
                                                                        <button type="button" href="#" class="remove_diagnose" title="remove"
                                                                        data-id="{{$item->PatDiagnoseID}}"><i
                                                                        class="fa fa-remove"></i></button>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    <button class="btn btn-default add_diagnose" type="button"
                                                            style="float:right">Add
                                                        Diagnose
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <h3> {!! Form::label('PEC','Emergency Contact
                                                    Information', ['class' => 'control-label col-md-6']) !!}</h3>
                                    </div>
                                </div>

                                <div class="row {{ $errors->has('PEMopt') ? ' has-error' : '' }}">
                                    <div class="form-group">
                                        {!! Form::label('PEMopt','Emergency Opt
                                            Out', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-4">
                                            {!! Form::checkbox('PEMopt', 1) !!}
                                            I'm choosing not to provide an Emergency Contact at this time
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PEMopt') }}</small></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('PEMContact') ? ' has-error' : '' }}">
                                        {!! Form::label('PEMContact','Emergency Contact
                                      ', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('PEMContact', null, ['class' => 'form-control', 'id' => 'PEMContact', 'placeholder' => 'Emergency Contact']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PEMContact') }}</small></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('PEMPhone') ? ' has-error' : '' }}">
                                        {!! Form::label('PEMPhone','Emergency Phone
                                      ', ['class' => 'control-label col-md-2']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('PEMPhone', null, ['class' => 'form-control', 'id' => 'PEMPhone', 'placeholder' => 'Emergency Phone']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('PEMPhone') }}</small></span>
                                        </div>
                                    </div>
                                </div>

                                <!-- Form actions -->
                                <div class="form-group">
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-responsive btn-info btn-sm">Save
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- content -->
    </aside>
    <!-- right-side -->
    @include('guest.patients.modals.addreses_modal')
    @include('guest.patients.modals.diagnoses_modal')
    @include('guest.patients.modals.doctors_modal')
    @include('guest.patients.modals.medgroup_modal')
@endsection


@section('scripts')
    <script src="{{asset('js/vendors/bootstrap-multiselect/bootstrap-multiselect.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('js/vendors/bootstrap-multiselect/bootstrap-multiselect.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('js/vendors/moment/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/daterangepicker/daterangepicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/datetimepicker/bootstrap-datetimepicker.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('js/vendors/select2/select2.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/pages/custom_elements.js')}}" type="text/javascript"></script>
    <script>
        $('document').ready(function () {
            $(document).on('click', '.add_address', function () {
                var id = $('.PatID').data('id');
                $('#PatID').val(id);
                $('#addresses_modal').modal('show');
            });
            // $('#edit_address_PState').select2({
            //     placeholder: "select",
            //     theme: "bootstrap",
            //     allowClear: true,
            // });
            $(document).on('click', '.add_new_address', function () {
                var data = $('.form_address').serializeArray();
                var url = "{{route('patients.address.store')}}";
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: data,
                    success: function (data) {
                        var new_row = '<tr class="animated lightSpeedIn"><td>' + data.Paddrname + '</td><td>' + data.PAddress + '</td><td>' + data.Pcity + '</td><td>'
                            + data.PState + '</td><td>' + data.PZip + '</td><td>' + data.PPhone + '</td><td>   <button class="edit_address" type="button"\n' +
                            '                                                                            title="edit"\n' +
                            '                                                                            data-id="' + data.PaddrID + '"><i\n' +
                            '                                                                                class="fa fa-pencil"></i>\n' +
                            '                                                                    </button></td></tr>';
                        setTimeout(function () {
                            $('#patient_address_table').append(new_row);
                        }, 800);
                        $('#addresses_modal').modal('hide');
                    }
                });
            });

            $(document).on('click', '.update_address', function () {
                var data = $('.form_address_update').serializeArray();
                var url = "{{route('patients.address.update')}}";
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: data,
                    success: function (data) {
                        console.log(data);
                        var new_row = '<tr class="animated lightSpeedIn" id="' + data.PaddrID + '"><td>' + data.Paddrname + '</td><td>' + data.PAddress + '</td><td>' + data.Pcity + '</td><td>'
                            + data.PState + '</td><td>' + data.PZip + '</td><td>' + data.PPhone + '</td><td>   <button class="edit_address" type="button"\n' +
                            '                                                                            title="edit"\n' +
                            '                                                                            data-id="' + data.PaddrID + '"><i\n' +
                            '                                                                                class="fa fa-pencil"></i>\n' +
                            '                                                                    </button></td></tr>';
                        $("tr#" + data.PaddrID).replaceWith(new_row);
                        $('#edit_addresses_modal').modal('hide');
                    }
                });
            });

            $(document).on('click', '.update_doctor', function () {
                var data = $('.edit_form_doctor').serializeArray();
                var url = "{{route('patients.doctor.update')}}";
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: data,
                    success: function (data) {
                        console.log(data);
                        var new_row = '<tr class="animated lightSpeedIn" id="' + data[0].PatientDoctorID + '"><td>' + data[1].DocFirstName + '</td><td>' + data[1].DocEmail + '</td><td>' +
                            data[1].DocPhone + '</td><td>' + data[1].Type + '</td><td>   <button class="edit_doctor" type="button"\n' +
                            '                                                                            title="edit"\n' +
                            '                                                                            data-id="' + data[0].PatientDoctorID + '"><i\n' +
                            '                                                                                class="fa fa-pencil"></i>\n' +
                            '                                                                    </button></td></tr>';
                        $("tr#" +  data[0].PatientDoctorID).replaceWith(new_row);
                        $('#edit_doctor_modal').modal('hide');
                    }
                });
            });

            $(document).on('click', '.update_medgroup', function () {
                var data = $('.edit_medgroup_form').serializeArray();
                var url = "{{route('patients.medgroup.update')}}";
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: data,
                    success: function (data) {
                        var new_row = '<tr class="animated lightSpeedIn" id="' + data[0].patMedGroupID + '"><td>' + data[1].medGroupName + '</td><td>   <button class="edit_medgroup" type="button"\n' +
                            '                                                                            title="edit"\n' +
                            '                                                                            data-id="' + data[0].patMedGroupID + '"><i\n' +
                            '                                                                                class="fa fa-pencil"></i>\n' +
                            '                                                                    </button> <button type="button" href="#" class="remove_medgroup" title="remoce"\n' +
                            '                                                                        data-id="'+ data[0].patMedGroupID +'"><i\n' +
                            '                                                                        class="fa fa-remove"></i></button></td></tr>';
                        $("tr#" +  data[0].patMedGroupID).replaceWith(new_row);
                        $('#edit_medgroup_modal').modal('hide');
                    }
                });
            });

            $(document).on('click', '.update_diagnose', function () {
                var data = $('.edit_form_diagnose').serializeArray();
                var url = "{{route('patients.diagnose.update')}}";
                $('.help-block-diagnose').empty();
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: data,
                    success: function (data) {
                        if(data['success'] == 'failed'){
                            $('.help-block-diagnose').html('Already Exist!!!');
                        }else{
                            var new_row = '<tr class="animated lightSpeedIn" id="'+ data[0].PatDiagnoseID +'"><td>'+  data[1].DiagnoseCode +'</td><td>' + data[1].Description + '</td><td>   <button class="edit_diagnose" type="button"\n' +
                                '                                                                            title="edit"\n' +
                                '                                                                            data-id="' + data[0].PatDiagnoseID + '"><i\n' +
                                '                                                                                class="fa fa-pencil"></i>\n' +
                                '                                                                    </button><button type="button" href="#" class="remove_diagnose" title="remove"\n' +
                                '                            \'                                                                        data-id="'+ data[0].PatDiagnoseID +'"><i\n' +
                                '                            \'                                                                        class="fa fa-remove"></i></button></td></tr>';
                            $("tr#" +  data[0].PatDiagnoseID).replaceWith(new_row);
                            $('#edit_diagnoses_modal').modal('hide');
                        }

                    }
                });
            });

            $(document).on('click', '.add_new_doctor', function () {
                var data = $('.form_doctor').serializeArray();
                var url = "{{route('patients.doctor.store')}}";
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: data,
                    success: function (data) {
                        var new_row = '<tr class="animated lightSpeedIn"><td>' + data[1].DocFirstName + '</td><td>' + data[1].DocEmail + '</td><td>' +
                            data[1].DocPhone + '</td><td>' + data[1].Type + '</td><td>   <button class="edit_doctor" type="button"\n' +
                            '                                                                            title="edit"\n' +
                            '                                                                            data-id="' + data[0].PatientDoctorID + '"><i\n' +
                            '                                                                                class="fa fa-pencil"></i>\n' +
                            '                                                                    </button></td></tr>';
                        setTimeout(function () {
                            $('#patient_doctor_table').append(new_row);
                        }, 800);
                        $('#doctor_modal').modal('hide');
                    }
                });
            });

            $(document).on('click', '.add_new_medgroup', function () {
                var data = $('.medgroup_form').serializeArray();
                var url = "{{route('patients.medgroup.store')}}";
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: data,
                    success: function (data) {
                        var new_row = '<tr class="animated lightSpeedIn"><td>' + data[1].medGroupName + '</td><td>   <button class="edit_medgroup" type="button"\n' +
                            '                                                                            title="edit"\n' +
                            '                                                                            data-id="' + data[0].patMedGroupID + '"><i\n' +
                            '                                                                                class="fa fa-pencil"></i>\n' +
                            '                                                                    </button>  <button type="button" href="#" class="remove_medgroup" title="remoce"\n' +
                            '                                                                        data-id="'+  data[0].patMedGroupID  +'"><i\n' +
                            '                                                                        class="fa fa-remove"></i></button></td></tr>';
                        setTimeout(function () {
                            $('#patient_medgroup_table').append(new_row);
                        }, 800);
                        $('#medgroup_modal').modal('hide');
                    }
                });
            });

            $(document).on('click', '.add_new_diagnose', function () {
                var data = $('.form_diagnose').serializeArray();
                var url = "{{route('patients.diagnose.store')}}"
                $('.help-block-diagnose').empty();
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: data,
                    success: function (data) {
                        console.log(data['success'], data);
                        if(data['success'] == 'failed'){
                            $('.help-block-diagnose').html('Already Exist!!!');
                        }else{
                            var new_row = '<tr class="animated lightSpeedIn"><td>'+ data[1].DiagnoseCode +'</td><td>' + data[1].Description + '</td><td>   <button class="edit_diagnose" type="button"\n' +
                                '                                                                            title="edit"\n' +
                                '                                                                            data-id="' + data[0].PatDiagnoseID + '"><i\n' +
                                '                                                                                class="fa fa-pencil"></i>\n' +
                                '                                                                    </button><button type="button" href="#" class="remove_diagnose" title="remove"\n' +
                                '                                                                        data-id="'+ data[0].PatDiagnoseID +'"><i\n' +
                                '                                                                        class="fa fa-remove"></i></button></td></tr>';
                            setTimeout(function () {
                                $('#patient_diagnose_table').append(new_row);
                            }, 800);
                            $('#diagnoses_modal').modal('hide');
                        }

                    }
                });
            });

            $(document).on('click', '.add_doctor', function () {
                var id = $('.PatID').data('id');
                $('#add_doctor_PatID').val(id);
                $('#doctor_modal').modal('show');
            });


            $(document).on('click', '.add_medGroup', function () {
                var id = $('.PatID').data('id');
                $('#medgroup_PatID').val(id);
                $('#medgroup_modal').modal('show');
            });

            $(document).on('click', '.add_diagnose', function () {
                var id = $('.PatID').data('id');
                $('#diagnose_PatID').val(id);
                $('#diagnoses_modal').modal('show');
            });

            $('.datetime4').datetimepicker({
                format: 'MM/DD/YYYY'
            });

            $(document).on('click', '.edit_address', function () {
                var id = $(this).data('id');
                var url = "{{route('patients.address.edit', ":id")}}";
                url = url.replace(':id', id);
                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function (data) {
                        var id;
                        $.each(data, function (i, item) {
                            console.log(i);
                            id = 'edit_address_' + i;
                            document.getElementById(id).value = item;
                        });
                        $('#edit_addresses_modal').modal('show');
                    }
                });
            });

            $(document).on('click', '.edit_doctor', function () {
                var id = $(this).data('id');
                var url = "{{route('patients.doctor.edit', ":id")}}";
                url = url.replace(':id', id);
                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function (data) {
                        var id;
                        $.each(data, function (i, item) {
                            console.log(i, item);
                            id = 'edit_doctor_' + i;
                            document.getElementById(id).value = item;
                        });
                        $('#edit_doctor_modal').modal('show');
                    }
                });
            });

            $(document).on('click', '.edit_medgroup', function () {
                var id = $(this).data('id');
                var url = "{{route('patients.medgroup.edit', ":id")}}";
                url = url.replace(':id', id);
                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function (data) {
                        var id;
                        $.each(data, function (i, item) {
                            console.log(i, item);
                            id = 'edit_medgroup_' + i;
                            document.getElementById(id).value = item;
                        });
                        $('#edit_medgroup_modal').modal('show');
                    }
                });
            });

            $(document).on('click', '.edit_diagnose', function () {
                var id = $(this).data('id');
                var url = "{{route('patients.diagnose.edit', ":id")}}";
                url = url.replace(':id', id);
                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function (data) {
                        var id;
                        $.each(data, function (i, item) {
                            console.log(i, item);
                            id = 'edit_diagnose_' + i;
                            document.getElementById(id).value = item;
                        });
                        $('#edit_diagnoses_modal').modal('show');
                    }
                });
            });

            $(document).on('change', '.diagnose_select', function () {
                var code = $(this).val();
                var url = "{{route('patients.diagnose.description', ":code")}}";
                url = url.replace(':code', code);
                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function (data) {
                            $('.diagnose_description').val(data);
                    }
                });
            });

            $(document).on('click', '.remove_diagnose', function(){
                var this_diagnose = $(this);
                var id = $(this).data('id');
                var url = "{{route('patients.diagnose.remove', ":id")}}";
                url = url.replace(':id', id);
                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function () {
                        this_diagnose.parents('tr').remove();
                    }
                });
            });

            $(document).on('click', '.remove_medgroup', function(){
                var this_diagnose = $(this);
                var id = $(this).data('id');
                var url = "{{route('patients.medgroup.remove', ":id")}}";
                url = url.replace(':id', id);
                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function () {
                        this_diagnose.parents('tr').remove();
                    }
                });
            });
        });
    </script>
@endsection