@extends('layouts.app')

@section('styles')
    <!--page level css -->
    <link href="{{asset('css/vendors/fullcalendar/fullcalendar.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/pages/calendar_custom.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" media="all" href="{{asset('css/vendors/bower-jvectormap/jquery-jvectormap-1.2.2.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/vendors/animate/animate.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('css/vendors/datetimepicker/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/pages/only_dashboard.css')}}"/>
    <!--end of page level css-->
@endsection

@section('content')
    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <div class="alert alert-success alert-dismissable margin5">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Success:</strong> You have successfully logged in.
        </div>
        <!-- Main content -->
        {{--<section class="content-header">--}}
            {{--<h1>Welcome to Dashboard</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li class="active">--}}
                    {{--<a href="#">--}}
                        {{--<i class="livicon" data-name="home" data-size="14" data-color="#333" data-hovercolor="#333"></i>--}}
                        {{--Dashboard--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        <section class="content">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
                    <!-- Trans label pie charts strats here-->
                    <div class="lightbluebg no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 text-right">
                                        <span>Views Today</span>
                                        <div class="number" id="myTargetElement1"></div>
                                    </div>
                                    <i class="livicon  pull-right" data-name="eye-open" data-l="true" data-c="#fff"
                                       data-hc="#fff" data-s="70"></i>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <small class="stat-label">Last Week</small>
                                        <h4 id="myTargetElement1.1"></h4>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <small class="stat-label">Last Month</small>
                                        <h4 id="myTargetElement1.2"></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInUpBig">
                    <!-- Trans label pie charts strats here-->
                    <div class="redbg no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 pull-left">
                                        <span>Today's Sales</span>
                                        <div class="number" id="myTargetElement2"></div>
                                    </div>
                                    <i class="livicon pull-right" data-name="piggybank" data-l="true" data-c="#fff"
                                       data-hc="#fff" data-s="70"></i>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <small class="stat-label">Last Week</small>
                                        <h4 id="myTargetElement2.1"></h4>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <small class="stat-label">Last Month</small>
                                        <h4 id="myTargetElement2.2"></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 margin_10 animated fadeInDownBig">
                    <!-- Trans label pie charts strats here-->
                    <div class="goldbg no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 pull-left">
                                        <span>Subscribers</span>
                                        <div class="number" id="myTargetElement3"></div>
                                    </div>
                                    <i class="livicon pull-right" data-name="archive-add" data-l="true" data-c="#fff"
                                       data-hc="#fff" data-s="70"></i>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <small class="stat-labelRealtime Server Load">Last Week</small>
                                        <h4 id="myTargetElement3.1"></h4>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <small class="stat-label">Last Month</small>
                                        <h4 id="myTargetElement3.2"></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInRightBig">
                    <!-- Trans label pie charts strats here-->
                    <div class="palebluecolorbg no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 pull-left">
                                        <span>Registered Users</span>
                                        <div class="number" id="myTargetElement4"></div>
                                    </div>
                                    <i class="livicon pull-right" data-name="users" data-l="true" data-c="#fff"
                                       data-hc="#fff" data-s="70"></i>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <small class="stat-label">Last Week</small>
                                        <h4 id="myTargetElement4.1"></h4>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <small class="stat-label">Last Month</small>
                                        <h4 id="myTargetElement4.2"></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row ">
                <div class="col-md-4 col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading border-light">
                            <h4 class="panel-title">
                                <i class="livicon" data-name="mail" data-size="18" data-color="white" data-hc="white"
                                   data-l="true"></i> Quick Mail
                            </h4>
                        </div>
                        <div class="panel-body no-padding">
                            <div class="compose row">
                                <label class="col-md-3 hidden-xs">To:</label>
                                <input type="text" class="col-md-9 col-xs-9" placeholder="name@email.com "
                                       tabindex="1"/>
                                <div class="clear"></div>
                                <label class="col-md-3 hidden-xs">Subject:</label>
                                <input type="text" class="col-md-9 col-xs-9" tabindex="1" placeholder="Subject"/>
                                <div class="clear"></div>
                                <div class='box-body'>
                                    <form>
        <textarea class="textarea textarea_home resize_vertical"
                  placeholder="Write mail content here"></textarea>
                                    </form>
                                </div>
                                <br/>
                                <div class="pull-right">
                                    <a href="#" class="btn btn-danger">Send</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <div class="panel panel-border">
                        <div class="panel-heading">
                            <h4 class="panel-title pull-left visitor">
                                <i class="livicon" data-name="map" data-size="16" data-loop="true" data-c="#515763"
                                   data-hc="#515763"></i> Visitors Map
                            </h4>
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="livicon" data-name="settings" data-size="16" data-loop="true"
                                       data-c="#515763" data-hc="#515763"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a class="panel-collapse collapses" href="#">
                                            <i class="fa fa-angle-up"></i>
                                            <span>Collapse</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="panel-refresh" href="#">
                                            <i class="fa fa-refresh"></i>
                                            <span>Refresh</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="panel-config" href="#panel-config" data-toggle="modal">
                                            <i class="fa fa-wrench"></i>
                                            <span>Configurations</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="panel-expand" href="#">
                                            <i class="fa fa-expand"></i>
                                            <span>Fullscreen</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body nopadmar">
                            <div id="world-map-markers" style="width:100%; height:300px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
@endsection

@section('scripts')
    <!-- begining of page level js -->
    <!-- EASY PIE CHART JS -->
    <script src="{{asset('js/vendors/jquery.easy-pie-chart/easypiechart.min.js')}}"></script>
    <script src="{{asset('js/vendors/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
    <script src="{{asset('js/jquery.easingpie.js')}}"></script>
    <!--end easy pie chart -->
    <!--for calendar-->
    <script src="{{asset('js/vendors/moment/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
    <!--   Realtime Server Load  -->
    <script src="{{asset('js/vendors/flotchart/jquery.flot.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/flotchart/jquery.flot.resize.js')}}" type="text/javascript"></script>
    <!--Sparkline Chart-->
    <script src="{{asset('js/vendors/sparklinecharts/jquery.sparkline.js')}}"></script>
    <!-- Back to Top-->
    <script type="text/javascript" src="{{asset('js/vendors/countUp.js/countUp.js')}}"></script>
    <!--   maps -->
    <script src="{{asset('js/vendors/bower-jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{asset('js/vendors/bower-jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('js/vendors/chartjs/Chart.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('js/vendors/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
    <!--  todolist-->
    <script src="{{asset('js/pages/todolist.js')}}"></script>
    <script src="{{asset('js/pages/dashboard.js')}}" type="text/javascript"></script>
@endsection
