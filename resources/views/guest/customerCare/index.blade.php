@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/datatables/dataTables.bootstrap.css')}}"/>
    <link href="{{asset('css/pages/tables.css')}}" rel="stylesheet" type="text/css"/>
@endsection


@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
            {{--<!--section starts-->--}}
            {{--<h1>Customer Care</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li>--}}
                    {{--<a href="{{route('home')}}">--}}
                        {{--<i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="active">Customer Care</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        <!--section ends-->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box default">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff"
                                   data-hc="white"></i>
                            </div>

                            <div class="caption" style="float: right;">
                                <a href="#" data-toggle="modal" data-target="#modal-1"
                                   style="text-decoration: none;color: white;"><i class="livicon" data-name="plus-alt"
                                                                                  data-size="16" data-loop="true"
                                                                                  data-c="#fff" data-hc="white"></i>Create
                                    New Ticket</a>
                            </div>
                        </div>
                        <div class="portlet-body">

                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
            <div class="modal fade modal-fade-in-scale-up" tabindex="-1" id="modal-1" role="dialog"
                 aria-labelledby="modalLabelfade" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h4 class="modal-title" id="modalLabelfade">Create New Ticket</h4>
                        </div>
                        <div class="modal-body">
                            <h3>
                                Are you filing a compliment or a concern?
                            </h3>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <h5><a href="{{route('customer_care.create_compliment')}}">Compliment</a></h5>
                                    <h5><a href="{{route('customer_care.create_concern')}}">Concern</a></h5>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- content -->
    </aside>
    <!-- right-side -->
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/dataTables.responsive.js')}}"></script>
    <script src="{{asset('js/pages/table-responsive.js')}}"></script>
@endsection