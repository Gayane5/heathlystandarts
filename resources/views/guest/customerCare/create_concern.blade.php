@extends('layouts.app')

@section('styles')
    <link href="{{asset('css/vendors/daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/vendors/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/vendors/clockface/clockface.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/vendors/jasny-bootstrap/jasny-bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('css/vendors/iCheck/all.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
            {{--<!--section starts-->--}}
            {{--<h1>Customer Care</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li>--}}
                    {{--<a href="{{route('home')}}">--}}
                        {{--<i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="active">Customer Care</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        <!--section ends-->
        <section class="content">
            <!--main content-->
            <div class="row">
                <!--row starts-->
                <div class="col-md-12">
                    <div class="panel panel-info" id="hidepanel2">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="plus" data-size="16" data-loop="true" data-c="#fff"
                                   data-hc="white"></i>
                            </h3>
                            <span class="pull-right">
                                    <i class="glyphicon glyphicon-chevron-up clickable"></i>
                                    <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                                </span>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" action="#" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="col-md-5 control-label" for=""><h3><b>General Information
                                                </b></h3>
                                        </label>
                                    </div>
                                    <!-- Name input-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="firstname">Incident Date:</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="livicon" data-name="calendar" data-size="14" data-loop="true"></i>
                                                </div>
                                                <input type="text" class="form-control datetime4" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Email input-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="lastname">Patient Name:</label>
                                        <div class="col-md-9">
                                            <input id="name" name="nalastnameme" type="text"
                                                   placeholder="Patient Name" class="form-control"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label" for=""><h3><b> Reporting Information
                                                </b></h3>
                                        </label>
                                    </div>
                                    <!-- Name input-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="firstname">Relationship</label>
                                        <div class="col-md-3">
                                            <select id="select21" class="form-control select2">
                                                <option value="">Select one...</option>
                                                <option value="AZ">Family</option>
                                                <option value="CO">Hospice Partner</option>
                                                <option value="ID">Patient</option>
                                                <option value="MT">Caregiver/Facility</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Email input-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="lastname">Name</label>
                                        <div class="col-md-9">
                                            <input id="name" name="name" type="text"
                                                   placeholder="Name" class="form-control"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="lastname">Phone</label>
                                        <div class="col-md-9">
                                            <input id="phone" name="phone" type="text"
                                                   placeholder="Phone" class="form-control"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="lastname">Hospice</label>
                                        <div class="col-md-9">
                                            <input id="hospice" name="hospice" type="text"
                                                   placeholder="Hospice" class="form-control"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label" for=""><h3><b> Order Information
                                                </b></h3>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="firstname">Order Type</label>
                                        <div class="col-md-3">
                                            <select id="select21" class="form-control select2">
                                                <option value="">Select one...</option>
                                                <option value="AZ">Delivery</option>
                                                <option value="CO">Pickup</option>
                                                <option value="ID">Service</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="firstname">Order Priority</label>
                                        <div class="col-md-3">
                                            <select id="select21" class="form-control select2">
                                                <option value="">Select one...</option>
                                                <option value="AZ">State</option>
                                                <option value="CO">Normal</option>
                                                <option value="ID">Future Dated</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-5 control-label" for=""><h3><b>  Equipment Type
                                                </b></h3>
                                        </label>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="copy"></label>
                                        <div class="col-md-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;Oxygen
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp; Lifts and Trapeze
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;Beds
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;Miscellaneous
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;Respiratory
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">Pediatrics
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;Wheelchairs
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp; Specialty Chairs
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp; Bathroom Aids
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="copy">If misc, describe</label>
                                        <div class="col-md-9">
                                            <input id="order_info" name="order_info" type="text"
                                                   placeholder="Order Info" class="form-control"></div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-5 control-label" for=""><h3><b>   Nature of Feedback
                                                </b></h3>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="copy"></label>
                                        <div class="col-md-2">
                                            <p>Equipment</p>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;Quality
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp; Cleanliness
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;  Not Operational
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <p>Technician</p>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;Attitude
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;  Lack of Follow-Up
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;  Improper Training
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <p>Customer Service</p>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;Attitude
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;  Lack of Follow-Up
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;   Extended Hold/Wait Time
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <p>Order Timing</p>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;Delay in Delivery
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;   Too Early - Before Patient was Ready
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="custom-checkbox"
                                                           value="">&nbsp;    Did Not Occur
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label" for=""><h3><b> Comments
                                                </b></h3>
                                        </label>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="firstname"></label>
                                        <div class="col-md-9">

                                            <textarea name="comments" class="form-control" rows="10"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label" for=""><h3><b> Attach File
                                                </b></h3>
                                        </label>
                                    </div>



                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="copy">Optional - (PDF, DOC, JPEG only, under 1MB)</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="..."></span>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label" for=""><h3><b>Copy Another Person
                                                </b></h3>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="copy">Optional - (Email address)</label>
                                        <div class="col-md-9">
                                            <input id="copy" name="copy" type="text"
                                                   placeholder="Copy" class="form-control"></div>
                                        </div>

                                    <!-- Form actions -->
                                    <div class="form-group">
                                        <div class="col-md-12 text-right">
                                            <button type="submit" class="btn btn-responsive btn-info btn-sm">Save
                                            </button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- content -->
    </aside>
    <!-- right-side -->
@endsection

@section('scripts')
    <script src="{{asset('js/vendors/moment/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/daterangepicker/daterangepicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/datetimepicker/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/clockface/clockface.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/jasny-bootstrap/jasny-bootstrap.js')}}"></script>
    <script src="{{asset('js/vendors/iCheck/icheck.js')}}"></script>
    <script src="{{asset('js/pages/form_examples.js')}}"></script>
    <script src="{{asset('js/pages/datepicker.js')}}" type="text/javascript"></script>
@endsection
