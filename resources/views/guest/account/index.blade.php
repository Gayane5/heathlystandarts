@extends('layouts.app')

@section('styles')
    <link href="{{asset('css/vendors/jasny-bootstrap/jasny-bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/vendors/x-editable/bootstrap-editable.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/vendors/bootstrap-multiselect/bootstrap-multiselect.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/select2/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/iCheck/all.css')}}" rel="stylesheet" type="text/css">
    <Link href="{{asset('css/vendors/iCheck/line/line.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/bootstrap-switch/bootstrap-switch.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/switchery/switchery.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}">
    <link href="{{asset('css/pages/formelements.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/pages/customform_elements.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/pages/radio_checkbox.css')}}">
    <link href="{{asset('css/pages/user_profile.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
            {{--<!--section starts-->--}}
            {{--<h1>User Profile</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li>--}}
                    {{--<a href="index.html">--}}
                        {{--<i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">User</a>--}}
                {{--</li>--}}
                {{--<li class="active">User Profile</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        <!--section ends-->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav  nav-tabs ">
                        <li class="active">
                            <a href="#tab1" data-toggle="tab">
                                <i class="livicon" data-name="user" data-size="16" data-c="#000" data-hc="#000" data-loop="true"></i> User Profile</a>
                        </li>
                        <li>
                            <a href="#tab2" data-toggle="tab">
                                <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i> Change Password</a>
                        </li>
                    </ul>
                    <div class="tab-content mar-top">
                        <div id="tab1" class="tab-pane fade active in">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <div class="col-md-4">
                                                <div class="text-center">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail img-file">
                                                            <img src="img/authors/avatar3.png" width="200" class="img-responsive" height="150" alt="riot">
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail img-max">
                                                        </div>
                                                        <div>
                                                                <span class="btn btn-default btn-file ">
                                                                <span class="fileinput-new">Select image</span>
                                                                <span class="fileinput-exists">Change</span>
                                                                <input type="file" name="...">
                                                                </span>
                                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped" id="users">
                                                            <tr>
                                                                <td>First Name</td>
                                                                <td>
                                                                    <a href="#" data-pk="1" class="editable" data-title="Edit User Name">User FirstName</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Last Name</td>
                                                                <td>
                                                                    <a href="#" data-pk="1" class="editable" data-title="Edit User Name">User LastName</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>E-mail</td>
                                                                <td>
                                                                    <a href="#" data-pk="1" class="editable" data-title="Edit E-mail">gankunding@hotmail.com</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Phone Number</td>
                                                                <td>
                                                                    <a href="#" data-pk="1" class="editable" data-title="Edit Phone Number">(999) 999-9999</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Address</td>
                                                                <td>
                                                                    <a href="#" data-pk="1" class="editable" data-title="Edit Address">Sydney, Australia</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Quick Entry (State)</td>
                                                                <td>
                                                                            <select id="select21" class="form-control select2">
                                                                                <option value="">Select value...</option>
                                                                                <optgroup label="Alaskan/Hawaiian Time Zone">
                                                                                    <option value="AK">Alaska</option>
                                                                                    <option value="HI">Hawaii</option>
                                                                                </optgroup>
                                                                                <optgroup label="Pacific Time Zone">
                                                                                    <option value="CA">California</option>
                                                                                    <option value="NV">Nevada</option>
                                                                                    <option value="OR">Oregon</option>
                                                                                    <option value="WA">Washington</option>
                                                                                </optgroup>
                                                                                <optgroup label="Mountain Time Zone">
                                                                                    <option value="AZ">Arizona</option>
                                                                                    <option value="CO">Colorado</option>
                                                                                    <option value="ID">Idaho</option>
                                                                                    <option value="MT">Montana</option>
                                                                                    <option value="NE">Nebraska</option>
                                                                                    <option value="NM">New Mexico</option>
                                                                                    <option value="ND">
                                                                                        North Dakota
                                                                                    </option>
                                                                                    <option value="UT">Utah</option>
                                                                                    <option value="WY">Wyoming</option>
                                                                                </optgroup>
                                                                                <optgroup label="Central Time Zone">
                                                                                    <option value="AL">Alabama</option>
                                                                                    <option value="AR">Arkansas</option>
                                                                                    <option value="IL">Illinois</option>
                                                                                    <option value="IA">Iowa</option>
                                                                                    <option value="KS">Kansas</option>
                                                                                    <option value="KY">Kentucky</option>
                                                                                    <option value="LA">Louisiana</option>
                                                                                    <option value="MN">Minnesota</option>
                                                                                    <option value="MS">
                                                                                        Mississippi
                                                                                    </option>
                                                                                    <option value="MO">Missouri</option>
                                                                                    <option value="OK">Oklahoma</option>
                                                                                    <option value="SD">
                                                                                        South Dakota
                                                                                    </option>
                                                                                    <option value="TX">Texas</option>
                                                                                    <option value="TN">Tennessee</option>
                                                                                    <option value="WI">Wisconsin</option>
                                                                                </optgroup>
                                                                                <optgroup label="Eastern Time Zone">
                                                                                    <option value="CT">
                                                                                        Connecticut
                                                                                    </option>
                                                                                    <option value="DE">Delaware</option>
                                                                                    <option value="FL">Florida</option>
                                                                                    <option value="GA">Georgia</option>
                                                                                    <option value="IN">Indiana</option>
                                                                                    <option value="ME">Maine</option>
                                                                                    <option value="MD">Maryland</option>
                                                                                    <option value="MA">
                                                                                        Massachusetts
                                                                                    </option>
                                                                                    <option value="MI">Michigan</option>
                                                                                    <option value="NH">
                                                                                        New Hampshire
                                                                                    </option>
                                                                                    <option value="NJ">New Jersey</option>
                                                                                    <option value="NY">New York</option>
                                                                                    <option value="NC">
                                                                                        North Carolina
                                                                                    </option>
                                                                                    <option value="OH">Ohio</option>
                                                                                    <option value="PA">
                                                                                        Pennsylvania
                                                                                    </option>
                                                                                    <option value="RI">
                                                                                        Rhode Island
                                                                                    </option>
                                                                                    <option value="SC">
                                                                                        South Carolina
                                                                                    </option>
                                                                                    <option value="VT">Vermont</option>
                                                                                    <option value="VA">Virginia</option>
                                                                                    <option value="WV">
                                                                                        West Virginia
                                                                                    </option>
                                                                                </optgroup>
                                                                            </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Always Email Me an
                                                                    Order Confirmation for Orders I Place</td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" class="" />
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab2" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-12 pd-top">
                                    <form action="#" class="form-horizontal">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label for="inputpassword" class="col-md-3 control-label">
                                                    Password
                                                    <span class='require'>*</span>
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                                            </span>
                                                        <input type="password" id="inputpassword" placeholder="Password" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputnumber" class="col-md-3 control-label">
                                                    Confirm Password
                                                    <span class='require'>*</span>
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                                            </span>
                                                        <input type="password" id="inputnumber" placeholder="Confirm Password" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                &nbsp;
                                                <input type="reset" class="btn btn-default hidden-xs" value="Reset">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- content -->
    </aside>
@endsection

@section('scripts')
    <script src="{{asset('js/vendors/jasny-bootstrap/jasny-bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/jquery-mockjax/jquery.mockjax.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/x-editable/bootstrap-editable.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/bootstrap-multiselect/bootstrap-multiselect.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/select2/select2.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/selectize/standalone/selectize.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/iCheck/icheck.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('js/vendors/bootstrap-switch/bootstrap-switch.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/vendors/switchery/switchery.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/vendors/bootstrap-maxlength/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/vendors/card/lib/jquery.card.js')}}"></script>
    <script src="{{asset('js/pages/custom_elements.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('js/pages/radio_checkbox.js')}}"></script>
    <script src="{{asset('js/pages/user_profile.js')}}" type="text/javascript"></script>
@endsection