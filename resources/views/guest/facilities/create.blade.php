@extends('layouts.app')

@section('styles')
    <link href="{{asset('css/vendors/bootstrap-multiselect/bootstrap-multiselect.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/select2/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/pages/customform_elements.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
            {{--<!--section starts-->--}}
            {{--<h1>Add New Facility</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li>--}}
                    {{--<a href="{{route('home')}}">--}}
                        {{--<i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="{{route('facilities.index')}}">--}}
                        {{--<i class="livicon" data-name="move" data-size="14" data-loop="true"></i> Facilities--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="active">Add new facility</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        <!--section ends-->
        <section class="content">
            <!--main content-->
            <div class="row">
                <!--row starts-->
                <div class="col-md-12">
                    <!--lg-6 starts-->
                    <!--basic form starts-->
                    <div class="panel panel-primary" id="hidepanel1">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="plus" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            </h3>
                            <span class="pull-right">
                                    <i class="glyphicon glyphicon-chevron-up clickable"></i>
                                    <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                                </span>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" action="#" method="post">
                                <fieldset>
                                    <!-- Name input-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="name">Name</label>
                                        <div class="col-md-9">
                                            <input id="name" name="name" type="text" placeholder="Name" class="form-control"></div>
                                    </div>
                                    <!-- Email input-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="address">Address</label>
                                        <div class="col-md-9">
                                            <input id="address" name="address" type="text" placeholder="Address" class="form-control"></div>
                                    </div>
                                    <!-- Phone input-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="city">City</label>
                                        <div class="col-md-9">
                                            <input id="city" name="city" type="text" placeholder="City" class="form-control"></div>
                                    </div>
                                    <!-- Phone input-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="state">State</label>
                                        <div class="col-md-9">
                                            <select id="select21" class="form-control select2">
                                                <option value="">Select value...</option>
                                                <optgroup label="Alaskan/Hawaiian Time Zone">
                                                    <option value="AK">Alaska</option>
                                                    <option value="HI">Hawaii</option>
                                                </optgroup>
                                                <optgroup label="Pacific Time Zone">
                                                    <option value="CA">California</option>
                                                    <option value="NV">Nevada</option>
                                                    <option value="OR">Oregon</option>
                                                    <option value="WA">Washington</option>
                                                </optgroup>
                                                <optgroup label="Mountain Time Zone">
                                                    <option value="AZ">Arizona</option>
                                                    <option value="CO">Colorado</option>
                                                    <option value="ID">Idaho</option>
                                                    <option value="MT">Montana</option>
                                                    <option value="NE">Nebraska</option>
                                                    <option value="NM">New Mexico</option>
                                                    <option value="ND">
                                                        North Dakota
                                                    </option>
                                                    <option value="UT">Utah</option>
                                                    <option value="WY">Wyoming</option>
                                                </optgroup>
                                                <optgroup label="Central Time Zone">
                                                    <option value="AL">Alabama</option>
                                                    <option value="AR">Arkansas</option>
                                                    <option value="IL">Illinois</option>
                                                    <option value="IA">Iowa</option>
                                                    <option value="KS">Kansas</option>
                                                    <option value="KY">Kentucky</option>
                                                    <option value="LA">Louisiana</option>
                                                    <option value="MN">Minnesota</option>
                                                    <option value="MS">
                                                        Mississippi
                                                    </option>
                                                    <option value="MO">Missouri</option>
                                                    <option value="OK">Oklahoma</option>
                                                    <option value="SD">
                                                        South Dakota
                                                    </option>
                                                    <option value="TX">Texas</option>
                                                    <option value="TN">Tennessee</option>
                                                    <option value="WI">Wisconsin</option>
                                                </optgroup>
                                                <optgroup label="Eastern Time Zone">
                                                    <option value="CT">
                                                        Connecticut
                                                    </option>
                                                    <option value="DE">Delaware</option>
                                                    <option value="FL">Florida</option>
                                                    <option value="GA">Georgia</option>
                                                    <option value="IN">Indiana</option>
                                                    <option value="ME">Maine</option>
                                                    <option value="MD">Maryland</option>
                                                    <option value="MA">
                                                        Massachusetts
                                                    </option>
                                                    <option value="MI">Michigan</option>
                                                    <option value="NH">
                                                        New Hampshire
                                                    </option>
                                                    <option value="NJ">New Jersey</option>
                                                    <option value="NY">New York</option>
                                                    <option value="NC">
                                                        North Carolina
                                                    </option>
                                                    <option value="OH">Ohio</option>
                                                    <option value="PA">
                                                        Pennsylvania
                                                    </option>
                                                    <option value="RI">
                                                        Rhode Island
                                                    </option>
                                                    <option value="SC">
                                                        South Carolina
                                                    </option>
                                                    <option value="VT">Vermont</option>
                                                    <option value="VA">Virginia</option>
                                                    <option value="WV">
                                                        West Virginia
                                                    </option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Phone input-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="zip">Zip</label>
                                        <div class="col-md-9">
                                            <input id="zip" name="zip" type="text" placeholder="Zip" class="form-control"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="phone">Phone</label>
                                        <div class="col-md-9">
                                            <input id="phone" name="phone" type="text" placeholder="Phone" class="form-control"></div>
                                    </div>
                                    <!-- Form actions -->
                                    <div class="form-group">
                                        <div class="col-md-12 text-right">
                                            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Save</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- content -->
    </aside>
    <!-- right-side -->
@endsection

@section('scripts')
    <script src="{{asset('js/vendors/bootstrap-multiselect/bootstrap-multiselect.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendors/select2/select2.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/pages/custom_elements.js')}}" type="text/javascript"></script>
@endsection