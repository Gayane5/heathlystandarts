@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/datatables/dataTables.bootstrap.css')}}" />
    <link href="{{asset('css/pages/tables.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
            {{--<!--section starts-->--}}
            {{--<h1>Facilities</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li>--}}
                    {{--<a href="{{route('home')}}">--}}
                        {{--<i class="livicon" data-name="move" data-size="14" data-loop="true"></i> Dashboard--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="active">Facilities</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        <!--section ends-->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box default">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="livicon" data-name="plus-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Facilities
                            </div>

                            <div class="caption" style="float: right;">
                                <a href="{{route('facilities.create')}}" style="text-decoration: none;color: white;"><i class="livicon" data-name="plus-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Add New Facility</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Facility</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><a href="{{route('facilities.edit', 1)}}">Country Villa Los Feliz Nursing Center</a></td>
                                    <td>3002 Rowena Ave.</td>
                                    <td>Los Angeles</td>
                                    <td>CA</td>
                                    <td><a href="#">Delete</a></td>
                                </tr>
                                <tr>
                                    <td><a href="{{route('facilities.edit', 1)}}">Country Villa Los Feliz Nursing Center</a></td>
                                    <td>3002 Rowena Ave.</td>
                                    <td>Los Angeles</td>
                                    <td>CA</td>
                                    <td><a href="#">Delete</a></td>
                                </tr>
                                <td><a href="{{route('facilities.edit', 1)}}">Country Villa Los Feliz Nursing Center</a></td>
                                <td>3002 Rowena Ave.</td>
                                <td>Los Angeles</td>
                                <td>CA</td>
                                <td><a href="#">Delete</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </section>
        <!-- content -->
    </aside>
    <!-- right-side -->
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/dataTables.responsive.js')}}"></script>
    <script src="{{asset('js/pages/table-responsive.js')}}"></script>
@endsection