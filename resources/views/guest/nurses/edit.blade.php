@extends('layouts.app')

@section('styles')
    <link href="{{asset('css/vendors/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/vendors/select2/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
            {{--<!--section starts-->--}}
            {{--<h1>Edit Nurse</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li>--}}
                    {{--<a href="{{route('home')}}">--}}
                        {{--<i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="{{route('nurses.index')}}">--}}
                        {{--<i class="livicon" data-name="move" data-size="14" data-loop="true"></i> Nurses--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="active">Edit nurse</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        <!--section ends-->
        <section class="content">
            <!--main content-->
            <div class="row">
                <!--row starts-->
                <div class="col-md-12">
                    <!--lg-6 starts-->
                    <!--basic form starts-->
                    <div class="panel panel-primary" id="hidepanel1">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="plus" data-size="16" data-loop="true" data-c="#fff"
                                   data-hc="white"></i>
                            </h3>
                            <span class="pull-right">
                                    <i class="glyphicon glyphicon-chevron-up clickable"></i>
                                </span>
                        </div>
                        <div class="panel-body" style="text-align: center;">
                            {!! Form::model($nurse, ['method' => 'post', 'url' => route('patients.update', $nurse->DocID)]) !!}
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="row">
                                    <div class="form-group {{ $errors->has('DocFirstName') ? ' has-error' : '' }}">
                                        {!! Form::label('DocFirstName','First Name', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::text('DocFirstName', null, ['class' => 'form-control', 'id' => 'DocFirstName', 'placeholder' => 'First Name']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('DocFirstName') }}</small></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('DocLastName') ? ' has-error' : '' }}">
                                        {!! Form::label('DocLastName','Last Name', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::text('DocLastName', null, ['class' => 'form-control', 'id' => 'DocLastName', 'placeholder' => 'Last Name']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('DocLastName') }}</small></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Email input-->
                                <div class="row">
                                    <div class="form-group {{ $errors->has('DocEmail') ? ' has-error' : '' }}">
                                        {!! Form::label('DocEmail','E-mail', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::text('DocEmail', null, ['class' => 'form-control', 'id' => 'DocEmail', 'placeholder' => 'Email']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('DocEmail') }}</small></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Phone input-->
                                <div class="row">
                                    <div class="form-group {{ $errors->has('DocPhone') ? ' has-error' : '' }}">
                                        {!! Form::label('DocPhone','Phone', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::text('DocPhone', null, ['class' => 'form-control', 'id' => 'DocPhone', 'placeholder' => 'Phone']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('DocPhone') }}</small></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('DocFax') ? ' has-error' : '' }}">
                                        {!! Form::label('DocFax','Fax', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::text('DocFax', null, ['class' => 'form-control', 'id' => 'DocFax', 'placeholder' => 'Fax']) !!}
                                            <span class="help-block">
                                            <small>{{ $errors->first('DocFax') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group  {{ $errors->has('DocAddress') ? ' has-error' : '' }}">
                                        {!! Form::label('DocAddress','Address', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::text('DocAddress', null, ['class' => 'form-control', 'id' => 'DocAddress', 'placeholder' => 'Address']) !!}
                                            <span class="help-block">
                                                    <small>{{ $errors->first('DocAddress') }}</small></span></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group {{ $errors->has('DocCity') ? ' has-error' : '' }}">
                                        {!! Form::label('DocCity','City', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::text('DocCity', null, ['class' => 'form-control', 'id' => 'DocCity', 'placeholder' => 'City']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('DocCity') }}</small></span></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group {{ $errors->has('DocState') ? ' has-error' : '' }}">
                                        {!! Form::label('DocState','State', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::select('DocState', $states ,null, ['class' => 'select2-target form-control', 'id' => 'select21', 'placeholder' => 'State']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('DocState') }}</small></span></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group {{ $errors->has('DocCountry') ? ' has-error' : '' }}">
                                        {!! Form::label('DocCountry','Country', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::text('DocCountry', null, ['class' => 'form-control', 'id' => 'DocCountry', 'placeholder' => 'Country']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('DocCountry') }}</small></span></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group {{ $errors->has('DocZip') ? ' has-error' : '' }}">
                                        {!! Form::label('DocZip','Zip', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::text('DocZip', null, ['class' => 'form-control', 'id' => 'DocZip', 'placeholder' => 'Zip']) !!}
                                            <span class="help-block">
                                                <small>{{ $errors->first('DocZip') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('DocCode') ? ' has-error' : '' }}">
                                        {!! Form::label('DocCode','Code', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::text('DocCode', null, ['class' => 'form-control', 'id' => 'DocCode', 'placeholder' => 'Code']) !!}
                                            <span class="help-block">
                                                    <small>{{ $errors->first('DocCode') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('DocSpec') ? ' has-error' : '' }}">
                                        {!! Form::label('DocSpec','Spec', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::text('DocSpec', null, ['class' => 'form-control', 'id' => 'DocSpec', 'placeholder' => 'Spec']) !!}
                                            <span class="help-block">
                                                    <small>{{ $errors->first('DocSpec') }}</small></span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('Type') ? ' has-error' : '' }}">
                                        {!! Form::label('Type','Type', ['class' => 'control-label col-md-3']) !!}
                                        <div class="col-md-6">
                                            {!! Form::select('Type', \App\Models\Nurses::$types, null, ['class' => 'form-control select2', 'id' => 'select22', 'placeholder' => 'Type']) !!}
                                            <span class="help-block">
                                                        <small>{{ $errors->first('Type') }}</small></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Form actions -->
                                <div class="form-group">
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Save
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- content -->
    </aside>
    <!-- right-side -->
@endsection

@section('scripts')
    <script src="{{asset('js/vendors/select2/select2.js')}}" type="text/javascript"></script>
    <script>
        // $(document).ready(function () {
        //     $("#select21").select2({
        //         theme: "bootstrap",
        //         placeholder: "Select a State"
        //     });
        //     $("#select22").select2({
        //         theme: "bootstrap",
        //         placeholder: "Select a Type"
        //     });
        // });
    </script>
@endsection