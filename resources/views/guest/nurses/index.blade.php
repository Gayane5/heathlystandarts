@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/datatables/dataTables.bootstrap.css')}}"/>
    <link href="{{asset('css/pages/tables.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
            {{--<!--section starts-->--}}
            {{--<h1>Nurses</h1>--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li>--}}
                    {{--<a href="{{route('home')}}">--}}
                        {{--<i class="livicon" data-name="move" data-size="14" data-loop="true"></i> Dashboard--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="active">Nurses</li>--}}
            {{--</ol>--}}
        {{--</section>--}}
        <!--section ends-->
        @if(session('success'))
            <div class="alert alert-success  fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <p class="text-center ">{{session('success')}}</p>
            </div>
        @endif
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box default">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="livicon" data-name="move" data-size="16" data-loop="true" data-c="#fff"
                                   data-hc="white"></i> Nurses
                            </div>

                            <div class="caption" style="float: right;">
                                <a href="{{route('nurses.create')}}" style="text-decoration: none;color: white;"><i
                                            class="livicon" data-name="plus-alt" data-size="16" data-loop="true"
                                            data-c="#fff" data-hc="white"></i> Add New Nurse</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Nurse</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($nurses as $nurse)
                                    <tr data-id="{{$nurse->DocID}}">
                                        <td>
                                            <a href="{{route('nurses.edit', $nurse->DocID)}}">{{$nurse->DocFirstName}} {{$nurse->DocLastName}}</a>
                                        </td>
                                        <td>{{$nurse->DocEmail}}</td>
                                        <td>{{$nurse->DocPhone}}</td>
                                        <td><a href="#" class="delete-nurse-row">Delete</a>
                                            <div class="modal fade" id="delete_confirm" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">
                                                                &times;
                                                            </button>
                                                        </div>
                                                        <div class="modal-body" style="text-align: center;">
                                                            <h4>Are you sure to delete this Nurse?</h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                            <a href="#" type="button"
                                                               class="btn btn-danger delete_confirm">Delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </section>
        <!-- content -->
    </aside>
    <!-- right-side -->
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/table-responsive.js')}}"></script>
<script>
    $(document).on('click','.delete-nurse-row',function () {
        $('.active-delete').removeClass('active-delete');
        $(this).parents('tr').addClass('active-delete');
        $('#delete_confirm').modal('show');
        $('.delete_confirm').click(function () {
            var id = $('.active-delete').data('id');
            var url = "{{route('nurses.delete', ":id")}}";
            url = url.replace(':id', id);
            $.ajax({
                method: 'get',
                url: url,
                success: function (data) {
                    if(data.success == null){
                        $('#delete_confirm').modal('hide');
                        $('.active-delete').addClass('animated lightSpeedOut');
                        setTimeout(function () {
                            $('.active-delete').remove();
                        },800);

                    }
                }
            });
        });
    });
</script>
@endsection