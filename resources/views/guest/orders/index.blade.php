@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/datatables/dataTables.bootstrap.css')}}"/>
    <link href="css/pages/tables.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <aside class="right-side">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box primary">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="livicon" data-name="open" data-size="16" data-loop="true" data-c="#fff"
                                   data-hc="white"></i> Open Orders
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Order ID</th>
                                        <th style="text-align: center">Patient</th>
                                        <th>Date Of Birth</th>
                                        <th>Time</th>
                                        <th>Medical ID</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($open))
                                        @foreach($open as $order)
                                            <tr>
                                                <td style="width: 10px"><a href="#" data-order="{{json_encode($order->authItems)}}" data-order="{{json_encode($order)}}" class="show_ordered_items">{{$order->AuthID}}</a></td>
                                                <td><a href="{{route('patients.history', $order->patient->PatID)}}">{{$order->patient ? $order->patient->PFirstName : ''}} {{$order->patient ? $order->patient->PLastName : ''}}</a></td>
                                                <td>{{$order->patient ? date('m/d/Y', strtotime($order->patient->PDOB)) : ''}}</td>
                                                <td>{{date('H:m:i', strtotime($order->AuthDate))}}</td>
                                                <td>{{$order->patient ? $order->patient->PMedicalID : ''}}</td>
                                                <td>
                                                    <span class="label label-sm label-danger"><a href="#" style="color: white;" data-order="{{json_encode($order->authItems)}}" class="show_ordered_items">{{$order->Status}}</a></span>
                                                    <span class="label label-sm label-default"><a style="color: white;"  href="{{route('cancel_order', $order->AuthID)}}" >Cancel</a></span>
                                                    <span class="label label-sm label-warning"><a style="color: white;"  href="{{route('edit_order', $order->AuthID)}}" >Edit</a></span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box primary">
                        <div class="portlet-title" style="background-color: seagreen">
                            <div class="caption">
                                <i class="livicon" data-name="completed"  data-size="16" data-loop="true" data-c="#fff"
                                   data-hc="white"></i> Completed Orders
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div>
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Order ID</th>
                                        <th style="text-align: center">Patient</th>
                                        <th>Date Of Birth</th>
                                        <th>Time</th>
                                        <th>MedicalID</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($closed))
                                        @foreach($closed as $order)
                                            <tr>
                                                <td style="width: 10px"><a href="#" data-order="{{json_encode($order->authItems)}}" class="show_ordered_items">{{$order->AuthID}}</a></td>
                                                <td><a href="{{route('patients.history', $order->patient->PatID)}}">{{$order->patient ? $order->patient->PFirstName : ''}} {{$order->patient ? $order->patient->PLastName : ''}}</a></td>
                                                <td>{{$order->patient ? date('m/d/Y', strtotime($order->patient->PDOB)) : ''}}</td>
                                                <td>{{date('H:m:i', strtotime($order->AuthDate))}}</td>
                                                <td>{{$order->patient ? $order->patient->PMedicalID : ''}}</td>
                                                <td>
                                                    <span class="label label-sm label-danger"><a href="#"  style="color: white" data-order="{{json_encode($order->authItems)}}" class="show_ordered_items">{{$order->Status}}</a></span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- content -->
    </aside>
    <!-- right-side -->
    @include('guest.orders.modals.ordered_items')
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/vendors/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/table-responsive.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.show_ordered_items').click(function () {
                $('#kit_modal_description').html("");
                var id = $(this).data('id');
                var order=$(this).data('order');
                var kit_row = '';
                $.each(order, function (key, value) {
                    kit_row = kit_row + 'Count: ' + value.Qty + ' ' +  value.CPTDescription + '<br>';
                });
                $('#kit_modal_description').append(kit_row);
                $('#kit_modal').modal('show');
            });
        });
    </script>
@endsection
