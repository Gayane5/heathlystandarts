@include('partials.header')

@yield('styles')

@include('partials.navbar')

{{--@include('partials.sidebar')--}}

@yield('content')

@include('partials.javascripts')

@yield('scripts')

@include('partials.footer')