</head>

<body class="skin-josh">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div>
                <a href="{{route('home')}}" class="logo">
                    <img src="{{asset('/images/wide_logo.png')}}" style="width: 13%; float: left" alt="logo">
                </a>
            </div>
            <ul class="nav navbar-nav" id="myNav">
                {{--@if(auth()->user()->isAdmin())--}}
                    <li class="active">
                        <a class="nav_item" id="/currentOrders" href="{{route('admin.authorization.index')}}" style="color: white;">
                            <img src="{{asset('/images/icons/icon-dashboard.png')}}">
                            Authorization
                        </a>
                    </li>
                {{--@else--}}
                <li class="active">
                    <a class="nav_item" id="/currentOrders" href="{{route('current_orders')}}" style="color: white;">
                        <img src="{{asset('/images/icons/icon-dashboard.png')}}">
                        Orders
                    </a>
                </li>
                <li>
                    <a class="nav_item" id="/patients" href="{{route('patients.index')}}" style="color: white;">
                        <img src="{{asset('/images/icons/icon-PatientList.png')}}">
                        Patients
                    </a>
                </li>
                <li>
                    <a class="nav_item" id="/customerCare" href="{{route('customer_care.index')}}" style="color: white;">
                        <img src="{{asset('/images/icons/icon-customerCare.png')}}">
                        Customer Care
                    </a>
                </li>
                <li>
                    <a class="nav_item" id="/nurses" href="{{route('nurses.index')}}" style="color: white;">
                        <img src="{{asset('/images/icons/icon-Nurses.png')}}">
                        Nurses
                    </a>
                </li>
                    {{--@endif--}}
            </ul>
            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/img/authors/avatar3.png" width="35" class="img-circle img-responsive pull-left"
                                 height="35" alt="riot">
                            <div class="riot">
                                <div>
                                    {{auth()->user()->UserName}}
                                    <span>
<i class="caret"></i>
</span>
                                </div>
                            </div>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header bg-light-blue">
                                <img src="/img/authors/avatar3.png" width="90" class="img-circle img-responsive" height="90"
                                     alt="User Image"/>
                                <p class="topprofiletext">   {{auth()->user()->UserName}}</p>
                            </li>
                            <!-- Menu Body -->
                            <li role="presentation"></li>
                            <li>
                                <a href="{{route('account')}}">
                                    <i class="livicon" data-name="gears" data-s="18"></i> My Account
                                </a>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ url('/logout') }}" onclick="event.preventDefault();
document.getElementById('logout-form').submit();">
                                        <i class="livicon" data-name="sign-out" data-s="18"></i> Logout
                                    </a>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
{{--</header>--}}