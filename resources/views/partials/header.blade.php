<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Heathly Standarts</title>
    <link rel="shortcut icon" type="image/ico" href="{{asset('images/Capture.ico')}}"/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link href="{{asset('css/vendors/app.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/pages/custom.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/animate/animate.css')}}" rel="stylesheet" type="text/css" />
    <!-- end of global css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>