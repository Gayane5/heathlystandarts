
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <section class="sidebar ">
            <div class="page-sidebar  sidebar-nav">
                <div class="clearfix"></div>
                <!-- BEGIN SIDEBAR MENU -->
                <ul id="menu" class="page-sidebar-menu">
                    <li class="active">
                        <a href="{{route('current_orders')}}">
                            <img src="{{asset('/images/icons/icon-dashboard.png')}}">
                            <span class="title">Orders</span>
                            <span class="fa arrow"></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('patients.index')}}">
                            <img src="{{asset('/images/icons/icon-PatientList.png')}}">
                            <span class="title">Patients</span>
                            <span class="fa arrow"></span>
                        </a>
                        {{--<ul class="sub-menu">--}}
                            {{--<li>--}}
                                {{--<a href="{{route('patients.index')}}">--}}
                                    {{--<i class="fa fa-angle-double-right"></i> View All--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{route('patients.create')}}">--}}
                                    {{--<i class="fa fa-angle-double-right"></i> Add New--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{asset('/images/icons/icon-customerCare.png')}}">
                            <span class="title">Customer Care</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('customer_care.index')}}">
                                    <i class="fa fa-angle-double-right"></i> View All
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-double-right"></i> Add New
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="{{route('customer_care.create_concern')}}">
                                            <i class="fa fa-angle-double-right"></i> Concern
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('customer_care.create_compliment')}}">
                                            <i class="fa fa-angle-double-right"></i> Compliment
                                            <span class="fa arrow"></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    {{--<li>--}}
                        {{--<a href="#">--}}
                            {{--<img src="{{asset('/images/icons/icon-Facilities.png')}}">--}}
                            {{--<span class="title">Facilities</span>--}}
                            {{--<span class="fa arrow"></span>--}}
                        {{--</a>--}}
                        {{--<ul class="sub-menu">--}}
                            {{--<li>--}}
                                {{--<a href="{{route('facilities.index')}}">--}}
                                    {{--<i class="fa fa-angle-double-right"></i> View All--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{route('facilities.create')}}">--}}
                                    {{--<i class="fa fa-angle-double-right"></i> Add new--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    <li>
                        <a href="#">
                            <img src="{{asset('/images/icons/icon-Nurses.png')}}">
                            <span class="title">Nurses</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('nurses.index')}}">
                                    <i class="fa fa-angle-double-right"></i> View All
                                </a>
                                <a href="{{route('nurses.create')}}">
                                    <i class="fa fa-angle-double-right"></i> Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                    {{--<li>--}}
                        {{--<a href="{{route('resources')}}">--}}
                            {{--<img src="{{asset('/images/icons/icon-Guide.png')}}">--}}
                            {{--<span class="title">Resources</span>--}}
                            {{--<span class="fa arrow"></span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
        </section>
        <!-- /.sidebar -->
    </aside>