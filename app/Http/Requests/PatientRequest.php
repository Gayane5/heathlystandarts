<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'PFirstName' => 'required',
            'PLastName' => 'required',
            'PDOB' => 'required',
            'PSex' => 'required',
            'PAdmited' => 'required',
            'PHeight' => 'numeric|between:0,999',
            'PWeight' => 'numeric|min:0',
        ];
    }
}
