<?php

namespace App\Http\Controllers\Guest;

use App\Http\Requests\PatientAddressRequest;
use App\Http\Requests\PatientRequest;
use App\Models\AuthItems;
use App\Models\Authorization;
use App\Models\Categories;
use App\Models\DeliveredItems;
use App\Models\Diagnoses;
use App\Models\InsuranceMedGroup;
use App\Models\ItemCptMedGroup;
use App\Models\Items;
use App\Models\MedGroup;
use App\Models\Nurses;
use App\Models\OxygenLiterFlow;
use App\Models\PatientAddress;
use App\Models\PatientDiagnose;
use App\Models\PatientDoctor;
use App\Models\PatientInsurance;
use App\Models\PatientMedGroup;
use App\Models\Patients;
use App\Models\State;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PatientController extends Controller
{
    public function index()
    {
        $patients = Patients::whereHas('patientInsurance', function ($query) {
            $query->where('IsuranceID', '=', Auth::user()->insurance->InsuranceID);
        })->get();
        return view('guest.patients.index', compact('patients'));
    }

    public function create()
    {
        $oxygenLiterFlow = OxygenLiterFlow::pluck('LiterSize', 'OxygenLiterID');
        $doctors = Nurses::pluck('DocFirstName', 'DocID');
        $medGroup = MedGroup::pluck('MedGroupName', 'medGroupID');
        return view('guest.patients.create', compact('oxygenLiterFlow', 'doctors', 'medGroup'));
    }

    public function store(PatientRequest $request)
    {
        $data = array_except($request->all(), ['_token']);
        if (!isset($request->PediatricPatient)) {
            $PediatricPatient = 0;
            $data = array_merge($data, ['PediatricPatient' => $PediatricPatient]);
        }
        $PDOB = date('Y-m-d', strtotime($request->PDOB));
        $PAdmited = date('Y-m-d', strtotime($request->PAdmited));
        $data = array_merge($data, ['PDOB' => $PDOB, 'PAdmited' => $PAdmited]);

        $patient = array_merge($data, ['PCreateDate' => Carbon::now()]);
        $patient = Patients::create($patient);

        $insurance = new PatientInsurance();
        $insurance->PatID = $patient->PatID;
        $insurance->IsuranceID = Auth::user()->insurance->InsuranceID;
        $insurance->PolicyNumber = 0;
        $insurance->save();

//        if($request->PDoctor){
//            $p_doctor = new PatientDoctor();
//            $p_doctor->PatID = $patient->PatID;
//            $p_doctor->DocID = $request->PDoctor;
//            $p_doctor->save();
//        }
//
//        if($request->PMedGroup){
//            $p_medgroup = new PatientMedGroup();
//            $p_medgroup->PatID = $patient->PatID;
//            $p_medgroup->medGroupID = $request->PMedGroup;
//            $p_medgroup->save();
//        }

        return redirect(route('patients.index'))->with('success', 'New Patient was successfully added!!!');
    }

    public function history($id)
    {
        $patient = Patients::where('PatID', $id)->first();
        $orders = Authorization::where('PatID', $id)->get();

        $addresses = PatientAddress::where('PatID', $id)->pluck('PAddrID')->toArray();
        $inventory = DeliveredItems::whereIn('PAddrID', $addresses)->whereHas('rentHistory', function($query){
            $query->where('Rentend', '>=', Carbon::now()->toDateString())
                    ->where('Rentstart', '<=', Carbon::now()->toDateString());
        })->get();

        $deliveries = DeliveredItems::whereIn('PAddrID', $addresses)->whereHas('soldHistory')->get();
        return view('guest.patients.history', compact('patient', 'orders', 'inventory', 'deliveries'));
    }

    public function edit($id)
    {
        $oxygenLiterFlow = OxygenLiterFlow::pluck('LiterSize', 'OxygenLiterID');
        $doctors =         $doctors = Nurses::orderBy('DocID', 'desc')->whereHas('nursesInsurance', function ($query) use ($id) {
            $query->where('InsuranceID', '=', Auth::user()->insurance->InsuranceID);
        })->select(
            DB::raw("CONCAT(DocLastName,' ',DocFirstName, ' - ', Type) AS name"),'DocID')->pluck('name', 'DocID');
        $medGroup = MedGroup::pluck('MedGroupName', 'medGroupID');
        $diagnoses = Diagnoses::pluck('DiagnoseCode', 'DiagnoseCode');
        $diagnose_desc = Diagnoses::select('Description')->first();
        $states = State::pluck('StateName', 'StateName');
        $patient = Patients::where('PatID', '=', $id)->first();
        return view('guest.patients.edit', compact('patient', 'oxygenLiterFlow', 'diagnoses', 'doctors', 'medGroup', 'states', 'diagnose_desc'));
    }

    public function update(PatientRequest $request, $id)
    {
        $data = array_except($request->all(), ['_token']);
        if (!isset($request->PediatricPatient)) {
            $PediatricPatient = 0;
            $data = array_merge($data, ['PediatricPatient' => $PediatricPatient]);
        }

        $PDOB = date('Y-m-d', strtotime($request->PDOB));
        $PAdmited = date('Y-m-d', strtotime($request->PAdmited));
        $data = array_merge($data, ['PDOB' => $PDOB, 'PAdmited' => $PAdmited]);

        if (!isset($request->PEMopt)) {
            $PEMopt = 0;
            $data = array_merge($data, ['PEMopt' => $PEMopt]);
        }

        $patient = Patients::where('PatID', $id)->first()->update($data);
        return redirect(route('patients.index'))->with('success', 'New Patient was successfully edited!!!');
    }

    public function delete($id)
    {
        $p_doctor = PatientDoctor::where('PatID', '=', $id)->first();
        if ($p_doctor) {
            $p_doctor->delete();
        }

        $p_medgroup = PatientMedGroup::where('PatID', '=', $id)->first();
        if ($p_medgroup) {
            $p_medgroup->delete();
        }

        $p_insurance = PatientInsurance::where('PatID', '=', $id)->first();
        if ($p_insurance) {
            $p_insurance->delete();
        }
        $patient = Patients::where('PatID', '=', $id)->first();
        if ($patient) {
            $patient->delete();
        }
        return response()->json(['success']);
    }

    public function pickupCreate()
    {
        return view('guest.patients.pickup');
    }

    public function respiteCreate()
    {
        return view('guest.patients.respite');
    }

    public function moveCreate()
    {
        return view('guest.patients.move');
    }

    public function exchangeCreate()
    {
        return view('guest.patients.exchange');
    }

    public function deliveryCreate($id)
    {
//        $categories = Categories::with('subCategories')->whereHas('subCategories.items.itemCptMedGroup.insuranceMedGroup', function ($query){
//            $query->where('InsuranceID', '=', Auth::user()->insurance->InsuranceID);
//        })->get();

        $categories = Categories::all();
        $patient = Patients::where('PatID', $id)->first();
        $doctors = Nurses::orderBy('DocID', 'desc')->whereHas('patientDoctor', function ($query) use ($patient) {
            $query->where('PatID', '=', $patient->PatID);
        })->select(
            DB::raw("CONCAT(DocFirstName,' ',DocLastName) AS name"),'DocID')->pluck('name', 'DocID');

        $addresses = PatientAddress::where('PatID', $id)->select(
            DB::raw("CONCAT(Paddrname,': ',PAddress, ' ',Pcity, ' ', PState, ' ',PZip) AS address"),'PaddrID')->pluck('address', 'PaddrID');

        return view('guest.patients.delivery', compact('categories', 'patient', 'doctors', 'addresses'));
    }

    public function getItems($id)
    {
        $item = Categories::where('CatID', $id)->with('subCategories')->first()->toArray();
        return response()->json([mb_convert_encoding($item, 'UTF-8', 'UTF-8'), base64_encode($item['CatPicture'])]);
    }

    public function getItemDetail($id)
    {
        $item = Items::where('ItemID', $id)->with('kits')->first()->toArray();
        return response()->json([mb_convert_encoding($item, 'UTF-8', 'UTF-8'), base64_encode($item['ItemPic'])]);
    }

    public function getKitDetail($id)
    {
        $item = Items::where('ItemID', $id)->first()->toArray();
        return response()->json([mb_convert_encoding($item, 'UTF-8', 'UTF-8'), base64_encode($item['ItemPic'])]);
    }

    public function storeDelivery(Request $request, $id)
    {
        $doc = PatientDoctor::where('PatID', $id)->orderBy('PatientDoctorID', 'desc')->first();

        $authorization = new Authorization();
        $authorization->HMOInsuranceID = Auth::user()->insurance->InsuranceID;
        $authorization->IPAIsuranceID = Auth::user()->insurance->InsuranceID;
        $authorization->AuthDate = Carbon::now()->toDateTimeString();
        $authorization->ExpDate = Carbon::now()->toDateTimeString();
        $authorization->ReqDate = Carbon::now()->toDateTimeString();
        $authorization->Status = 'Pending';
        $authorization->UserID = Auth::user()->UserID;
        $authorization->PatID = $id;
        $authorization->RefDocID = $request['PDoctor'];
        $authorization->PCPDocID = isset($doc) ? $doc->DocID : null;
        $authorization->AuthorizationNumber = 1;
        $authorization->save();

        $medGroup = InsuranceMedGroup::where('InsuranceID', Auth::user()->insurance->InsuranceID)->first();
        foreach ($request->all() as $key => $item) {
            $medgroupitem = ItemCptMedGroup::where('ItemID', $key)->where('medGroupID', $medGroup->medGroupID)->first();
            if ($medgroupitem) {
                $authitems = new AuthItems();
                $authitems->CPTDescription = $medgroupitem->CPTDescription;
                $authitems->CPTCode = $medgroupitem->CPTCode;
                $authitems->CPTCode2 = $medgroupitem->CPTCode2;
                $authitems->Qty = $item ? $item : 1;
                $authitems->AuthID = $authorization->AuthID;
                $authitems->RentIndicator = $medgroupitem->RentIndicator;
                $authitems->FreqNumber = $medgroupitem->RentChargeFrequency;
                $authitems->FreqType = $medgroupitem->RentChargeFrequencyType;
                $authitems->save();
            }
        }
        return response()->json('success');
    }

    public function storeAddress(PatientAddressRequest $request)
    {
        $data = array_except($request->all(), ['_token']);
        $address = PatientAddress::create($data);
        return response()->json($address);
    }

    public function editAddress($id)
    {
        $address = PatientAddress::where('PaddrID', $id)->first();
        return response()->json($address);
    }

    public function editDoctor($id)
    {
        $address = PatientDoctor::where('PatientDoctorID', $id)->first();
        return response()->json($address);
    }

    public function editMedGroup($id)
    {
        $address = PatientMedGroup::where('patMedGroupID', $id)->first();
        return response()->json($address);
    }

    public function editDiagnose($id)
    {
        $address = PatientDiagnose::where('PatDiagnoseID', $id)->first();
        return response()->json($address);
    }

    public function getDiagnoseDescription($code)
    {
        $desc = Diagnoses::where('DiagnoseCode', $code)->first();
        return response()->json($desc->Description);
    }


    public function updateAddress(PatientAddressRequest $request)
    {
        $data = array_except($request->all(), ['_token', 'PaddrID']);
        $address = PatientAddress::where('PaddrID', $request->PaddrID)->first();
        $address = $address->update($data);
        return response()->json(PatientAddress::where('PaddrID', $request->PaddrID)->first());
    }

    public function updateDoctor(Request $request)
    {
        $data = array_except($request->all(), ['_token', 'PatientDoctorID']);
        $address = PatientDoctor::where('PatientDoctorID', $request->PatientDoctorID)->first();
        $address = $address->update($data);
        return response()->json([PatientDoctor::where('PatientDoctorID', $request->PatientDoctorID)->first(), PatientDoctor::where('PatientDoctorID', $request->PatientDoctorID)->first()->doctor]);
    }

    public function updateMedGroup(Request $request)
    {
        $data = array_except($request->all(), ['_token', 'patMedGroupID']);
        $address = PatientMedGroup::where('patMedGroupID', $request->patMedGroupID)->first();
        $address = $address->update($data);
        return response()->json([PatientMedGroup::where('patMedGroupID', $request->patMedGroupID)->first(), PatientMedGroup::where('patMedGroupID', $request->patMedGroupID)->first()->medGroup]);
    }

    public function updateDiagnose(Request $request)
    {
        $data = array_except($request->all(), ['_token', 'PatDiagnoseID', 'Description']);
        $exist = PatientDiagnose::where('PatID', $request->PatID)->where('DiagnoseCode', $request->DiagnoseCode)->first();

        if(!$exist){
            $diagnose = PatientDiagnose::where('PatDiagnoseID', $request->PatDiagnoseID)->first();
            $diagnose->DiagnoseCode  = $request->DiagnoseCode;
            $diagnose->save();
            return response()->json([PatientDiagnose::where('PatDiagnoseID', $request->PatDiagnoseID)->first(), PatientDiagnose::where('PatDiagnoseID', $request->PatDiagnoseID)->first()->diagnose]);
        }else{
            return response()->json(['success' => 'failed']);
        }

    }

    public function storeDoctor(Request $request)
    {
        $data = array_except($request->all(), ['_token']);
        $address = PatientDoctor::create($data);
        return response()->json([$address, $address->doctor]);
    }

    public function storeMedGroup(Request $request)
    {
        $data = array_except($request->all(), ['_token']);
        $address = PatientMedGroup::create($data);
        return response()->json([$address, $address->medGroup]);
    }

    public function storeDiagnose(Request $request)
    {
        $exist = PatientDiagnose::where('PatID', $request->PatID)->where('DiagnoseCode', $request->DiagnoseCode)->first();
        if(!$exist){
            $data = array_except($request->all(), ['_token']);
            $address = PatientDiagnose::create($data);
            return response()->json([$address, $address->diagnose]);
        }else{
            return response()->json(['success' => 'failed']);
        }

    }
    
    public function removeDiagnose($id)
    {
        $diagnose = PatientDiagnose::where('PatDiagnoseID', $id)->first();
        $diagnose->delete();
        return response()->json('success');
    }

    public function removeMedgroup($id){
        $diagnose = PatientMedGroup::where('patMedGroupID', $id)->first();
        $diagnose->delete();
        return response()->json('success');
    }
}
