<?php

namespace App\Http\Controllers\Guest;

use App\Models\AuthItems;
use App\Models\Authorization;
use App\Models\Categories;
use App\Models\InsuranceMedGroup;
use App\Models\ItemCptMedGroup;
use App\Models\Nurses;
use App\Models\PatientAddress;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PharIo\Manifest\AuthorTest;

class OrdersController extends Controller
{
    public function index()
    {
        $open = Authorization::where('IPAIsuranceID', Auth::user()->insurance->InsuranceID)->where('status', 'pending')->get();
        $closed = Authorization::where('IPAIsuranceID', Auth::user()->insurance->InsuranceID)->where('status', '!=' ,'pending')->get();
        return view('guest.orders.index', compact('open', 'closed'));
    }

    public function cancel($id)
    {
        $item = AuthItems::where('AuthID', $id)->first();
        if($item){
            $item->delete();
        }
        $order = Authorization::where('AuthID', $id)->first();
        if($order){
            $order->delete();
        }

        return redirect()->back()->with('Success', 'Order Cnaceled Successfully');
    }

    public function editOrder($id)
    {
        $order = Authorization::where('AuthID', $id)->first();

        $categories = Categories::all();
        $doctors = Nurses::orderBy('DocID', 'desc')->whereHas('patientDoctor', function ($query) use ($order) {
            $query->where('PatID', '=', $order->PatID);
        })->select(
            DB::raw("CONCAT(DocFirstName,' ',DocLastName) AS name"),'DocID')->pluck('name', 'DocID');

        $addresses = PatientAddress::where('PatID', $order->PatID)->select(
            DB::raw("CONCAT(Paddrname,': ',PAddress, ' ',Pcity, ' ', PState, ' ',PZip) AS address"),'PaddrID')->pluck('address', 'PaddrID');

        return view('guest.patients.delivery-edit', compact('categories', 'doctors', 'addresses', 'order'));
    }

    public function updateDelivery(Request $request, $id)
    {
        $order = Authorization::where('AuthID', $id)->first();
        $auth_items = AuthItems::where('AuthID', $id)->get();
        foreach ($auth_items as $item)
        {
            $item->delete();
        }

        $order->RefDocID = $request['PDoctor'];
        $order->save();

        $medGroup = InsuranceMedGroup::where('InsuranceID', Auth::user()->insurance->InsuranceID)->first();
        foreach ($request->all() as $key => $item) {
            $medgroupitem = ItemCptMedGroup::where('ItemID', $key)->where('medGroupID', $medGroup->medGroupID)->first();
            if ($medgroupitem) {
                $authitems = new AuthItems();
                $authitems->CPTDescription = $medgroupitem->CPTDescription;
                $authitems->CPTCode = $medgroupitem->CPTCode;
                $authitems->CPTCode2 = $medgroupitem->CPTCode2;
                $authitems->Qty = $item ? $item : 1;
                $authitems->AuthID = $id;
                $authitems->RentIndicator = $medgroupitem->RentIndicator;
                $authitems->FreqNumber = $medgroupitem->RentChargeFrequency;
                $authitems->FreqType = $medgroupitem->RentChargeFrequencyType;
                $authitems->save();
            }
        }

        return response()->json('success');

    }
}
