<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public function index()
    {
        return view('guest.customerCare.index');
    }

    public function createConcern ()
    {
        return view('guest.customerCare.create_concern');
    }

    public function createCompliment ()
    {
        return view('guest.customerCare.create_compliment');
    }
}
