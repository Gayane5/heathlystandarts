<?php

namespace App\Http\Controllers\Guest;

use App\Http\Requests\NursesRequest;
use App\Models\Nurses;
use App\Models\NursesInsurance;
use App\Models\PatientDoctor;
use App\Models\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use PharIo\Manifest\AuthorCollectionTest;

class NursesController extends Controller
{
    public function index()
    {
        $nurses = Nurses::whereHas('nursesInsurance', function ($query) {
            $query->where('InsuranceID', '=', Auth::user()->insurance->InsuranceID);
        })->get();

        return view('guest.nurses.index', compact('nurses'));
    }

    public function create()
    {
        $states = State::pluck('StateName', 'StateName');
        return view('guest.nurses.create', compact('states'));
    }

    public function store(NursesRequest $request)
    {
        $request->except('_token');

        $nurse = Nurses::create($request->all());

        $insurance = new NursesInsurance();
        $insurance->DocID = $nurse->DocID;
        $insurance->InsuranceID = Auth::user()->insurance->InsuranceID;
        $insurance->save();

        return redirect(route('nurses.index'))->with('success', 'New Nurse was successfully added!!!');
    }

    public function update(NursesRequest $request, $id)
    {

        $data = array_except($request->all(), ['_token']);
        $nurse = Nurses::where('DocID', $id)->update($data);

        return redirect(route('nurses.index'))->with('success', 'New Nurse was successfully updated!!!');
    }

    public function edit($id)
    {
        $nurse = Nurses::where('DocID', $id)->first();
        $states = State::pluck('StateName', 'StateName');
        return view('guest.nurses.edit', compact('nurse', 'states'));
    }

    public function delete($id)
    {
        $n_insurance = NursesInsurance::where('DocID', '=' ,$id)->first();
        if($n_insurance){
            $n_insurance->delete();
        }

        $nurse = Nurses::where('DocID', '=', $id)->first();
        if($nurse){
            $nurse->delete();
        }
        return response()->json(['success']);
    }
}
