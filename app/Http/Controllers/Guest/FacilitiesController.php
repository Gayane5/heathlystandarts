<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FacilitiesController extends Controller
{
    public function index()
    {
        return view('guest.facilities.index');
    }

    public function create()
    {
        return view('guest.facilities.create');
    }

    public function edit($id)
    {
        return view('guest.facilities.edit');
    }
}
