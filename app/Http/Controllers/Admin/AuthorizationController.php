<?php

namespace App\Http\Controllers\Admin;

use App\Models\AuthItems;
use App\Models\Authorization;
use App\Models\DeliveredItems;
use App\Models\Insurance;
use App\Models\ItemCptMedGroup;
use App\Models\RentedHistory;
use App\Models\RentInventory;
use App\Models\SoldHistory;
use App\Models\SoldInventory;
use App\Models\WareHouseItems;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class AuthorizationController extends Controller
{
    public function index()
    {
        $insurances = Insurance::all();
        $authorizations = Authorization::where('AuthDate', Carbon::now()->format('Y-m-d'))->get();
        $authItems = $authorizations->pluck('authItems');
        $left_items_array = [];
        $qty = [];
        foreach($authItems as $item){
            foreach ($item as $i){
                $qty[$i->AuthItemID] = $i->Qty;
            }
            $left_items_array = array_merge($left_items_array, $item->pluck('AuthItemID')->toArray());
        }

        return view('admin.authorization.index', compact('insurances', 'authorizations', 'left_items_array', 'qty'));
    }

    public function getItemInfo($id)
    {
        $item = AuthItems::where('AuthItemID', $id)->first();
        $item_cpt_med_group = ItemCptMedGroup::where('CPTDescription', $item->CPTDescription)->where('CPTCode', $item->CPTCode)->first();
        $wareHouseItems = WareHouseItems::where('ItemID', $item_cpt_med_group->ItemID)->get();
        return response()->json($wareHouseItems ? $wareHouseItems->toArray() : null);
    }

    public function changeStatus($id)
    {
        $item = Authorization::where('AuthID', $id)->first();
        $item->Status = 'Completed';
        $item->save();

        foreach ($item->authItems as $auth_item) {
            $deliveredItems = new DeliveredItems();
            $deliveredItems->DeliveryDate = Carbon::now()->toDateTimeString();
            if(isset($auth_item->itemCptMedGroup) && isset($auth_item->itemCptMedGroup->wareHouseItems)){
                $deliveredItems->WHItemID = $auth_item->itemCptMedGroup->wareHouseItems->WHItemID;
                $deliveredItems->ItemNotes = $auth_item->itemCptMedGroup->wareHouseItems->ItemNotes;
                $deliveredItems->SerialNO = $auth_item->itemCptMedGroup->wareHouseItems->SerialNO;
                $deliveredItems->PurchasedPrice = $auth_item->itemCptMedGroup->wareHouseItems->PurchasedPrice;
                $deliveredItems->InvoiceID = $auth_item->itemCptMedGroup->wareHouseItems->InvoiceID;
                if(isset($auth_item->itemCptMedGroup->wareHouseItems->orderedKitItems) && isset($auth_item->itemCptMedGroup->wareHouseItems->orderedKitItems->orderedItems)){
                    $deliveredItems->DelivererID = $auth_item->itemCptMedGroup->wareHouseItems->orderedKitItems->orderedItems->DelivererID;
                    $deliveredItems->PaddrID = $auth_item->itemCptMedGroup->wareHouseItems->orderedKitItems->orderedItems->deliveryOrder->PaddrID;
                    if(isset($auth_item->itemCptMedGroup->wareHouseItems->orderedKitItems->orderedItems->deliveryOrder)){
                        $deliveredItems->EXorDelOrderID = $auth_item->itemCptMedGroup->wareHouseItems->orderedKitItems->orderedItems->deliveryOrder->DelOrderID;
                        $deliveredItems->EXorDelOrderDate = $auth_item->itemCptMedGroup->wareHouseItems->orderedKitItems->orderedItems->deliveryOrder->OrderDate;
                    }else{
                        $deliveredItems->EXorDelOrderID = null;
                        $deliveredItems->EXorDelOrderDate = null;
                    }
                }else{
                    $deliveredItems->PaddrID = null;
                    $deliveredItems->DelivererID = null;
                }
            }else{
                $deliveredItems->PaddrID = null;
                $deliveredItems->EXorDelOrderID = null;
                $deliveredItems->EXorDelOrderDate = null;
                $deliveredItems->WHItemID = null;
                $deliveredItems->ItemNotes = null;
                $deliveredItems->SerialNO = null;
                $deliveredItems->PurchasedPrice = null;
                $deliveredItems->InvoiceID = null;
                $deliveredItems->DelivererID = null;
            }
            $deliveredItems->Notes = null;
            $deliveredItems->save();

            if ($auth_item->RentIndicator) {
                $rentInventory = new RentInventory();
                $rentInventory->Date = Carbon::now()->toDateTimeString();
                $rentInventory->RentNote = null;
                $rentInventory->StDate = Carbon::now()->toDateTimeString();
                $rentInventory->EndDate = Carbon::now()->toDateTimeString();
                $rentInventory->Frequency = $auth_item->itemCptMedGroup ? $auth_item->itemCptMedGroup->RentChargeFrequency : null;
                $rentInventory->FrequencyType = $auth_item->itemCptMedGroup ? $auth_item->itemCptMedGroup->RentChargeFrequencyType : null;
                $rentInventory->RentPrice = $auth_item->itemCptMedGroup ? $auth_item->itemCptMedGroup->CPTPrice : null;
                $rentInventory->KitorItemID = $auth_item->itemCptMedGroup && $auth_item->itemCptMedGroup->wareHouseItems && $auth_item->itemCptMedGroup->wareHouseItems->orderedKitItems && $auth_item->itemCptMedGroup->wareHouseItems->orderedKitItems->orderedItems ? $auth_item->itemCptMedGroup->wareHouseItems->orderedKitItems->orderedItems->KitID : null;
                $rentInventory->AuthItemID = $auth_item->AuthItemID;
                $rentInventory->CPTCode = $auth_item->CPTCode;
                $rentInventory->CPTCode2 = $auth_item->CPTCode2;
                $rentInventory->CPTDescription = $auth_item->CPTDescription;
                $rentInventory->save();

                $rentHistory = new RentedHistory();
                $rentHistory->DeliveryID = $deliveredItems->DeliveryID;
                $rentHistory->RentItemID = $rentInventory->RentItemID;
                $rentHistory->RentStart = Carbon::now()->toDateTimeString();
                $rentHistory->RentEnd = null;
                $rentHistory->save();
            } else {
                $soldInventory = new SoldInventory();
                $soldInventory->DeliveryCharge = Carbon::now()->toDateTimeString();
                $soldInventory->AuthItemID = $auth_item->AuthItemID;
                $soldInventory->ItemID = $item->AuthID;
                $soldInventory->CPTCode = $auth_item->CPTCode;
                $soldInventory->CPTCode2 = $auth_item->CPTCode2;
                $soldInventory->CPTDescription = $auth_item->CPTDescription;
                $soldInventory->SoldPrice = $auth_item->itemCptMedGroup ? $auth_item->itemCptMedGroup->CPTPrice : null;
                $soldInventory->SoldDate = Carbon::now()->toDateTimeString();
                $soldInventory->save();

                $soldHistory = new SoldHistory();
                $soldHistory->SoldID = $soldInventory->SoldID;
                $soldHistory->DeliveryID = $deliveredItems->DeliveryID;
                $soldHistory->save();
            }
        }
        return response()->json('success');
    }
}
