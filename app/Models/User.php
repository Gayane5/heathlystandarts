<?php

namespace App\Models;

use App\Models\Insurance;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends  Authenticatable
{
    use Notifiable;

    protected $table = "users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'UserName', 'Company', 'UserLogin', 'UserPassword' , 'UserEmail', 'Activation', 'UserLevelID',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $primaryKey = 'UserID';

    public function getAuthPassword()
    {
        return $this->UserPassword;
    }

    public function insurance()
    {
        return $this->hasOne(UserInsurance::class, 'UserID', 'UserID');
    }

    public function level()
    {
        return $this->belongsTo(UserLevels::class, 'UserLevelID', 'UserLevelID');
    }

    public function isAdmin()
    {
            if ($this->level->UserLevelName == 'Administrator')
            {
                return true;
            }

        return false;
    }
}
