<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemKits extends Model
{
    protected $table = 'itemkits';

    public function kit()
    {
        return $this->belongsTo(Items::class, 'ItemID', 'ItemID');
    }
}
