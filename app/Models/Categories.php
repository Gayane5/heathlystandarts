<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';

    public function subCategories()
    {
        return $this->hasMany(SubCategories::class, 'CatID', 'CatID')->with('items');
    }
}
