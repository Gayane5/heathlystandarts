<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderedKitItems extends Model
{
    protected $table = 'orderedkititems';

    public function orderedItems()
    {
        return $this->belongsTo(OrderedItems::class, 'OrdItmID', 'OrdItmID');
    }

}
