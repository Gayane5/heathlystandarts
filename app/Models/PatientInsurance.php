<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientInsurance extends Model
{
    protected $table = 'patientinsurance';

    public $timestamps = false;

    protected $primaryKey = "InsPatID";
}
