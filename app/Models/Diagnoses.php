<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Diagnoses extends Model
{
    protected $table = 'diagnose';

    protected $primaryKey = "diagnoseCode";
}
