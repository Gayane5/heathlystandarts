<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedGroup extends Model
{
    protected $table = 'medgroup';

    protected $primaryKey = "medGroupID";

}
