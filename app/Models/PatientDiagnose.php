<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientDiagnose extends Model
{
    protected $table = 'patientdiagnose';

    protected $primaryKey = "PatDiagnoseID";

    public $timestamps = false;

    protected $fillable = [
        'PatID', 'DiagnoseCode'
    ];

    public function diagnose()
    {
        return $this->hasOne(Diagnoses::class, 'DiagnoseCode', 'DiagnoseCode');
    }
}
