<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Authorization extends Model
{
    protected $table = 'authorization';

    protected $primaryKey = "AuthID";

    public $timestamps = false;

    public function patient()
    {
        return $this->belongsTo(Patients::class, 'PatID', 'PatID');
    }

    public function authItems()
    {
        return $this->hasMany(AuthItems::class, 'AuthID', 'AuthID');

    }

}
