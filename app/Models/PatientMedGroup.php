<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientMedGroup extends Model
{
    protected $table = 'patientmedgroup';

    protected $primaryKey = "patMedGroupID";

    public $timestamps = false;

    protected $fillable = [
        'PatID', 'medGroupID'
    ];

    public function medGroup()
    {
        return $this->hasOne(MedGroup::class, 'medGroupID', 'medGroupID');
    }
}
