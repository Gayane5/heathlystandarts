<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLevels extends Model
{
    protected $table = 'userlevels';

    protected $primaryKey = 'UserLevelID';
}
