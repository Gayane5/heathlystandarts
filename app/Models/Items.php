<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $table = 'items';

    public function subCategory()
    {
        return $this->belongsTo(SubCategories::class, 'SubCatID', 'SubCatID')->with('category');
    }

    public function itemCptMedGroup()
    {
        return $this->belongsTo(ItemCptMedGroup::class, 'ItemID', 'ItemID');
    }

    public function kits()
    {
        return $this->hasMany(ItemKits::class, 'KitID', 'ItemID')->with('kit');
    }

}
