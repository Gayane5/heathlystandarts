<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NursesInsurance extends Model
{
    protected $table = 'doctorinsurance';

    public $timestamps = false;

    protected $primaryKey = "DocInsID";

    public function nurses()
    {
        return $this->belongsToMany(Nurses::class);
    }

}
