<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderedItems extends Model
{
    protected $table = 'ordereditems';

    public $timestamps = false;


    public function deliveryOrder()
    {
        return $this->belongsTo(DeliveryOrder::class, 'DelOrderID', 'DelOrderID');
    }

}
