<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientDoctor extends Model
{
    protected $table = 'patientdoctor';

    protected $primaryKey = "PatientDoctorID";

    public $timestamps = false;

    protected $fillable = [
        'PatID', 'DocID'
    ];

    public function doctor()
    {
        return $this->hasOne(Nurses::class, 'DocID', 'DocID');
    }

}
