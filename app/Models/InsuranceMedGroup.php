<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InsuranceMedGroup extends Model
{
    protected $table = 'insurancemedgroup';

    protected $primaryKey = "InsMedGroupID";

    public function itemCptMedGroup()
    {
        return $this->hasMany(ItemCptMedGroup::class, 'medGroupID', 'medGroupID')->with('items');
    }
}
