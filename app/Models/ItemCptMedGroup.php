<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemCptMedGroup extends Model
{
    protected $table = 'itemcptmedgroup';

    protected $primaryKey = "CPTmedGroupID";

    public function items()
    {
        return $this->belongsTo(Items::class, 'ItemID', 'ItemID')->with('subCategory');
    }

    public function wareHouseItems()
    {
        return $this->belongsTo(WareHouseItems::class, 'ItemID', 'ItemID');
    }

    public function insuranceMedGroup()
    {
        return $this->belongsTo(InsuranceMedGroup::class, 'medGroupID', 'medGroupID');
    }
}
