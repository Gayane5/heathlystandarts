<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Nurses extends Model
{
    protected $table = 'doctor';

    public $timestamps = false;

    protected $primaryKey = "DocID";

    protected $fillable = [
        'DocFirstName', 'DocLastName', 'DocEmail', 'DocPhone', 'DocFax', 'DocAddress', 'DocCity', 'DocState',
        'DocCountry', 'DocCode', 'DocSpec', 'Type'
    ];

    public static $types = [
        'Doctor' => 'Doctor',
        'Nurse' => 'Nurse'
    ];

    protected $appends = [
        'full_name',
    ];

    public function getFullNameAttribute()
    {
        return ucfirst($this->DocFirstName) . ' ' . ucfirst($this->DocLastName);
    }

    public function nursesInsurance()
    {
        return $this->hasOne(NursesInsurance::class, 'DocID', 'DocID')->where('InsuranceID', Auth::user()->insurance->InsuranceID);
    }

    public function patientDoctor()
    {
        return $this->hasMany(PatientDoctor::class, 'DocID', 'DocID');
    }
}
