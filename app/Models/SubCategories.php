<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategories extends Model
{
    protected $table = 'subcategories';

    public function items()
    {
        return $this->hasMany(Items::class, 'SubCatID', 'SubCatID')->with('itemCptMedGroup')->with('kits');
    }

    public function category()
    {
        return $this->belongsTo(Categories::class, 'CatID', 'CatID');
    }
}
