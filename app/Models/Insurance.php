<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    protected $table = 'insurance';

    public function authorization()
    {
        return $this->hasMany(Authorization::class, 'HMOInsuranceID', 'InsuranceID');
    }
}
