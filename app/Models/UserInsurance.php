<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInsurance extends Model
{
    protected $table = 'userinsurance';
}
