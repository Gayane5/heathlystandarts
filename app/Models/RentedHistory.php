<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RentedHistory extends Model
{
    protected $table = 'rentedhistory';

    protected $primaryKey = "RentHistoryID";

    public $timestamps = false;
}
