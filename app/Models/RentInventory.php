<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RentInventory extends Model
{
    protected $table = 'rentinventory';

    public $timestamps = false;

    protected $primaryKey = 'RentItemID';
}
