<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveredItems extends Model
{
    protected $table = 'delivereditems';

    protected $primaryKey = "DeliveryID";

    public $timestamps = false;

    public function rentHistory()
    {
        return $this->hasMany(RentedHistory::class, 'DeliveryID', 'DeliveryID');
    }


    public function soldHistory()
    {
        return $this->hasMany(SoldHistory::class, 'DeliveryID', 'DeliveryID');
    }
}
