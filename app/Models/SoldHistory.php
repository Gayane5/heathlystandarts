<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SoldHistory extends Model
{
    protected $table = 'soldhistory';

    protected $primaryKey = "SoldHistoryID";

    public $timestamps = false;
}
