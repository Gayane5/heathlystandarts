<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OxygenLiterFlow extends Model
{
    protected $table = 'oxygenliterflow';

    protected $primaryKey = "OxigenLiterID";

}
