<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuthItems extends Model
{
    protected $table = 'authitems';

    protected $primaryKey = "AuthID";

    public $timestamps = false;

    public function item()
    {
        return $this->belongsTo( Items::class, 'CPTDescription', 'ItemName');
    }

    public function itemCptMedGroup()
    {
        return $this->belongsTo(ItemCptMedGroup::class, 'CPTDescription', 'CPTDescription');
    }

}
