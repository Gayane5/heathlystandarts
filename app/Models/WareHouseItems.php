<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WareHouseItems extends Model
{
    protected $table = 'warehouseitems';

    public function orderedKitItems()
    {
        return $this->belongsTo(OrderedKitItems::class, 'WHItemID', 'WHItemID');
    }
}
