<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientAddress extends Model
{
    protected $table = 'patientaddresses';

    protected $primaryKey = "PaddrID";

    public $timestamps = false;

    protected $fillable = [
        'PatID', 'Paddrname', 'PAddress', 'Pcity', 'PState', 'PZip', 'PPhone',
        'AddressNote', 'Status'
    ];
}
