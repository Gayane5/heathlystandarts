<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Patients extends Model
{
    protected $table = 'patient';

    protected $primaryKey = "PatID";

    public $timestamps = false;

    protected $fillable = [
        'PFirstName', 'PLastName', 'PDOB', 'PediatricPatient', 'DeliveryLocationID', 'PSex', 'PHeight', 'PWeight',
        'POxygenLiterID', 'PCPAP', 'PIPAP', 'PEPAP', 'PRate', 'PEMopt', 'PEMContact', 'PStatus', 'PAdmited', 'PEMPhone',
        'PLanguage', 'Insulin', 'CareGiverInfo', 'PCreateDate', 'PMedicalID'
    ];

    public static $PDOB_M = [
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December',
    ];

    public static $PDOB_D = [
        '01' => '01',
        '02' => '02',
        '03' => '03',
        '04' => '04',
        '05' => '05',
        '06' => '06',
        '07' => '07',
        '08' => '08',
        '09' => '09',
        '10' => '10',
        '11' => '11',
        '12' => '12',
        '13' => '13',
        '14' => '14',
        '15' => '15',
        '16' => '16',
        '17' => '17',
        '18' => '18',
        '19' => '19',
        '20' => '20',
        '21' => '21',
        '22' => '22',
        '23' => '23',
        '24' => '24',
        '25' => '25',
        '26' => '26',
        '27' => '27',
        '28' => '28',
        '29' => '29',
        '30' => '30',
        '31' => '31',
    ];

    public static $PDOB_Y = [
        "1900" => '1900',
        "1901" => '1901',
        "1902" => '1902',
        "1903" => '1903',
        "1904" => '1904',
        "1905" => '1905',
        "1906" => '1906',
        "1907" => '1907',
        "1908" => '1908',
        "1909" => '1909',
        "1910" => '1910',
        "1911" => '1911'
    ];

    public static $sex = [
        'M' => 'Male',
        'F' => 'Female',
        'O' => 'Other'
    ];

    public static $BIPAP = [
        '0' => '0',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
    ];

    public static $languages = [
        'English' => 'English'
    ];

    public function addresses()
    {
        return $this->hasMany(PatientAddress::class, 'PatID', 'PatID');
    }

    public function patientInsurance()
    {
        return $this->hasOne(PatientInsurance::class, 'PatID', 'PatID')->where('IsuranceID', Auth::user()->insurance->InsuranceID);
    }

    public function oxygenLiterFlow()
    {
        return $this->hasMany(OxygenLiterFlow::class, 'POxygenLiterID', 'OxygenLiterID');
    }

    public function doctors()
    {
        return $this->hasMany(PatientDoctor::class, 'PatID', 'PatID');
    }

    public function diagnoses()
    {
        return $this->hasMany(PatientDiagnose::class, 'PatID', 'PatID');
    }

    public function medGroups()
    {
        return $this->hasMany(PatientMedGroup::class, 'PatID', 'PatID');
    }

    public function authorizations()
    {
        return $this->hasMany(Authorization::class, 'PatID', 'PatID');
    }
}

